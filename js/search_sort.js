(function ($) {
  'use strict';

  // Add js behavior to sort widget.
  // Ensures that the search form is reloaded with sort parameters
  // on click.
  Drupal.behaviors.toolbarLabels = {
    attach: function (context, settings) {
      document.addEventListener('input', function (event) {
        // Only run for the sort widget.
        if (event.target.id !== 'search--sort-widget') return;
        window.location.href = event.target.value;
      }, false);
    }
  };

}(jQuery));
