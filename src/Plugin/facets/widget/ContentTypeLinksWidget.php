<?php

namespace Drupal\custom_search\Plugin\facets\widget;

use Drupal\Core\Url;
use Drupal\facets\FacetInterface;
use Drupal\facets\Plugin\facets\widget\LinksWidget;
use Drupal\facets\Result\Result;
use Drupal\facets\Result\ResultInterface;
use Drupal\custom_search\ContentTypeMapInterface;

/**
 * Custom Content Type links widget.
 *
 * @FacetsWidget(
 *   id = "custom_content_type_links",
 *   label = @Translation("List of links (Content Type only)"),
 *   description = @Translation("A widget that shows a hierarchical list of Content Type links."),
 * )
 */
class ContentTypeLinksWidget extends LinksWidget implements ContentTypeMapInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = $this->prebuild($facet);

    $this->appendWidgetLibrary($build);
    $soft_limit = (int) $this->getConfiguration()['soft_limit'];
    if ($soft_limit !== 0) {
      $show_less_label = $this->getConfiguration()['soft_limit_settings']['show_less_label'];
      $show_more_label = $this->getConfiguration()['soft_limit_settings']['show_more_label'];
      $build['#attached']['library'][] = 'facets/soft-limit';
      $build['#attached']['drupalSettings']['facets']['softLimit'][$facet->id()] = $soft_limit;
      $build['#attached']['drupalSettings']['facets']['softLimitSettings'][$facet->id()]['showLessLabel'] = $show_less_label;
      $build['#attached']['drupalSettings']['facets']['softLimitSettings'][$facet->id()]['showMoreLabel'] = $show_more_label;
    }

    $results = $facet->getResults();
    if ($this->getConfiguration()['show_reset_link'] && count($results) > 0 && (!$this->getConfiguration()['hide_reset_when_no_selection'] || $facet->getActiveItems())) {
      // Add reset link.
      $max_items = array_sum(array_map(function ($item) {
        return $item->getCount();
      }, $results));

      $urlProcessorManager = \Drupal::service('plugin.manager.facets.url_processor');
      $url_processor = $urlProcessorManager->createInstance($facet->getFacetSourceConfig()->getUrlProcessorName(), ['facet' => $facet]);
      $active_filters = $url_processor->getActiveFilters();

      if (isset($active_filters[''])) {
        unset($active_filters['']);
      }

      unset($active_filters[$facet->id()]);

      // Only if there are still active filters, use url generator.
      if ($active_filters) {
        $url = \Drupal::service('facets.utility.url_generator')
          ->getUrl($active_filters, FALSE);
      }
      else {
        $request = \Drupal::request();
        $url = Url::createFromRequest($request);
        $params = $request->query->all();
        unset($params[$url_processor->getFilterKey()]);
        $url->setOption('query', $params);
      }

      $result_item = new Result($facet, 'reset_all', $this->getConfiguration()['reset_text'], $max_items);
      $result_item->setActiveState(FALSE);
      $result_item->setUrl($url);

      $item = $this->prepareLink($result_item);

      // Add a class for the reset link wrapper.
      $item['#wrapper_attributes'] = ['class' => ['facet-item', 'facets-reset']];

      // Check if any other facet is in use.
      $none_active = TRUE;
      foreach ($results as $result) {
        if ($result->isActive()) {
          $none_active = FALSE;
          break;
        }
      }

      // Add an is-active class when no other facet is in use.
      if ($none_active) {
        $item['#attributes'] = ['class' => ['is-active']];
      }

      // Put reset facet link on first place.
      array_unshift($build['#items'], $item);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  private function prebuild(FacetInterface $facet) {
    $this->facet = $facet;

    $items = array_map(function (Result $result) use ($facet) {

      if (empty($result->getUrl())) {
        return $this->buildResultItem($result);
      }
      else {
        // When the facet is being build in an AJAX request, and the facetsource
        // is a block, we need to update the url to use the current request url.
        if ($result->getUrl()->isRouted() && $result->getUrl()->getRouteName() === 'facets.block.ajax') {
          $request = \Drupal::request();
          $url_object = \Drupal::service('path.validator')
            ->getUrlIfValid($request->getPathInfo());
          if ($url_object) {
            $url = $result->getUrl();
            $options = $url->getOptions();
            $route_params = $url_object->getRouteParameters();
            $route_name = $url_object->getRouteName();
            $result->setUrl(new Url($route_name, $route_params, $options));
          }
        }

        return $this->buildListItems($facet, $result);
      }
    }, $facet->getResults());

    $widget = $facet->getWidget();

    return [
      '#theme' => $this->getFacetItemListThemeHook($facet),
      '#facet' => $facet,
      '#items' => $items,
      '#attributes' => [
        'data-drupal-facet-id' => $facet->id(),
        'data-drupal-facet-alias' => $facet->getUrlAlias(),
      ],
      '#context' => ['list_style' => $widget['type']],
      '#cache' => [
        'contexts' => [
          'url.path',
          'url.query_args',
        ],
      ],
    ];
  }

  /**
   * Builds a renderable array of result items.
   *
   * @param \Drupal\facets\FacetInterface $facet
   *   The facet we need to build.
   * @param \Drupal\facets\Result\ResultInterface $result
   *   A result item.
   *
   * @return array
   *   A renderable array of the result.
   */
  protected function buildListItems(FacetInterface $facet, ResultInterface $result) {
    $collapsed_types = $this->getThirdLevelTypeKeys(self::CONTENT_TYPE_HIERARCHY);
    $classes = ['facet-item'];
    $items = $this->prepareLink($result);

    $children = $result->getChildren();

    // Check if we need to expand this result.
    if ($children && ($this->facet->getExpandHierarchy() || $result->isActive() || $result->hasActiveChildren())) {

      $child_items = [];

      foreach ($children as $child) {
        // Ensure that third level of Content Type facets is collapsed,
        // unless it is active.
        if (!in_array($result->getDisplayValue(), $collapsed_types)) {
          $child_items[] = $this->buildListItems($facet, $child);
          $classes[] = 'facet-item--expanded';
        }

        if (in_array($result->getDisplayValue(), $collapsed_types) && $result->isActive()) {
          $child_items[] = $this->buildListItems($facet, $child);
          $classes[] = 'facet-item--expanded';
        }
      }

      $items['children'] = [
        '#theme' => $this->getFacetItemListThemeHook($facet),
        '#items' => $child_items,
      ];

      if ($result->hasActiveChildren()) {
        $classes[] = 'facet-item--active-trail';
      }

    }
    else {
      if ($children) {
        $classes[] = 'facet-item--collapsed';
      }
    }

    if ($result->isActive()) {
      $items['#attributes']['class'][] = 'is-active';
    }

    $items['#wrapper_attributes'] = ['class' => $classes];
    $items['#attributes']['data-drupal-facet-item-id'] = $this->facet->getUrlAlias() . '-' . str_replace(' ', '-', $result->getRawValue());
    $items['#attributes']['data-drupal-facet-item-value'] = $result->getRawValue();
    return $items;
  }

  /**
   * Returns Content Type options that must be initially collapsed.
   *
   * @param array $array
   *   Array of content types structured hierarchically.
   *
   * @return array
   *   Array of Content Type labels to be initially collapsed.
   */
  protected function getThirdLevelTypeKeys(array $array) {
    $keys = [];
    foreach ($array as $subarray) {
      foreach ($subarray as $key => $array) {
        if (is_array($array) && !empty($array)) {
          $keys[] = $key;
        }
      }
    }

    return $keys;
  }

}
