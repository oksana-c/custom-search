<?php

namespace Drupal\custom_search\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a processor for dates.
 *
 * @FacetsProcessor(
 *   id = "custom_agency_hierarchy",
 *   label = @Translation("CLIENT Custom Agency processor"),
 *   description = @Translation("Display Agencies grouped by Gov Branch if content exists for those agencies."),
 *   stages = {
 *     "build" = 36
 *   }
 * )
 */
class CustomAgencyProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'custom_agency_hierarchy';
  }

}
