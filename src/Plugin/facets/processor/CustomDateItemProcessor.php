<?php

namespace Drupal\custom_search\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\custom_search\Plugin\facets\query_type\CustomDateRange;

/**
 * Provides a processor for dates.
 *
 * @FacetsProcessor(
 *   id = "custom_date_item",
 *   label = @Translation("CLIENT Custom Date item processor"),
 *   description = @Translation("Display dates range options if content exists for specified ranges."),
 *   stages = {
 *     "build" = 36
 *   }
 * )
 */
class CustomDateItemProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    return $results;
  }

  /**
   * Human readable array of date range options.
   *
   * @return array
   *   An array of range options.
   */
  private function rangeOptions() {
    return [
      CustomDateRange::FACETAPI_DATE_PAST_WEEK => $this->t('Past Week'),
      CustomDateRange::FACETAPI_DATE_PAST_MONTH => $this->t('Past Month'),
      CustomDateRange::FACETAPI_DATE_PAST_6_MONTHS => $this->t('Past 6 Months'),
      CustomDateRange::FACETAPI_DATE_PAST_YEAR => $this->t('Past Year'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $this->getConfiguration();

    $build['range'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Ranges included in a facet'),
      '#default_value' => $this->getConfiguration()['range'],
      '#options' => $this->rangeOptions(),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'custom_date_range';
  }

}
