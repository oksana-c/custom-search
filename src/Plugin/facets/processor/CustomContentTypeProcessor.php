<?php

namespace Drupal\custom_search\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * Provides a processor for dates.
 *
 * @FacetsProcessor(
 *   id = "custom_content_type_hierarchy",
 *   label = @Translation("CLIENT Custom Content Type processor"),
 *   description = @Translation("Display Content Type options if content exists for specified options."),
 *   stages = {
 *     "build" = 36
 *   }
 * )
 */
class CustomContentTypeProcessor extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType() {
    return 'custom_content_type_hierarchy';
  }

}
