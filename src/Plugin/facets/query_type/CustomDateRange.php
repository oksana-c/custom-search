<?php

namespace Drupal\custom_search\Plugin\facets\query_type;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\facets\QueryType\QueryTypeRangeBase;
use Drupal\facets\Result\Result;

/**
 * Support for CLIENT date facets within the Search API scope.
 *
 * @FacetsQueryType(
 *   id = "custom_date_range",
 *   label = @Translation("CLIENT Date Range"),
 * )
 */
class CustomDateRange extends QueryTypeRangeBase {

  /**
   * Constant for Past Week range.
   *
   * Last 7 days.
   */

  const FACETAPI_DATE_PAST_WEEK = 'past_week';
  /**
   * Constant for Past Month range.
   *
   * Last 30 days.
   */
  const FACETAPI_DATE_PAST_MONTH = 'past_month';

  /**
   * Constant for Past 6 Month range.
   *
   * Last 180 days.
   */
  const FACETAPI_DATE_PAST_6_MONTHS = 'past_6_months';

  /**
   * Constant for Past Year range.
   *
   * Last 365 days.
   */
  const FACETAPI_DATE_PAST_YEAR = 'past_year';

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $facet = $configuration['facet'];
    $processors = $facet->getProcessors();
    $dateProcessorConfig = $processors['custom_date_item']->getConfiguration();

    $configuration = $this->getConfiguration();
    $configuration['range'] = $dateProcessorConfig['range'];
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // If there were no results or no query object, we can't do anything.
    if (empty($this->results)) {
      return $this->facet;
    }

    $range = $this->getRange();

    $query_operator = $this->facet->getQueryOperator();
    $facet_results = [];
    $range_count = array_fill_keys($range, []);

    foreach ($this->results as $result) {
      // Go through the results and add facet results grouped by filters
      // defined by self::calculateResultFilter().
      if ($result['count'] || $query_operator == 'or') {
        $count = $result['count'];
        if ($result_filter = $this->calculateResultFilter(trim($result['filter'], '"'))) {

          $range_intersect = array_intersect($range, $result_filter['range']);

          if (isset($facet_results[$result_filter['raw']]) && !empty($range_intersect)) {
            $facet_results[$result_filter['raw']]->setCount(
              $facet_results[$result_filter['raw']]->getCount() + $count
            );
            foreach ($result_filter['range'] as $item) {
              $range_count[$item]['raw'] = !empty($range_count[$item]['raw']) ? $range_count[$item]['raw'] :
                $result_filter['raw'];
              $range_count[$item]['count'] = $range_count[$item]['count'] + $count;
            }
          }
          elseif (!empty($range_intersect)) {
            $facet_results[$result_filter['raw']] = new Result($this->facet, $result_filter['raw'], $result_filter['display'], $count);
            foreach ($result_filter['range'] as $item) {
              $range_count[$item]['raw'] = !empty($range_count[$item]['raw']) ? $range_count[$item]['raw'] :
                $result_filter['raw'];
              $range_count[$item]['count'] = (isset($range_count[$item]['count'])) ? $range_count[$item]['count'] + $count : 0;
            }
          }
        }
      }
    }

    // Update range counts according to totals across similar ranges.
    foreach ($range_count as $range_data) {
      if (isset($range_data['raw'])) {
        $facet_results[$range_data['raw']]->setCount($range_data['count']);
      }
    }
    $this->facet->setResults($facet_results);
    return $this->facet;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRange($value) {
    $start = NULL;
    $stop = NULL;
    if (strpos($value, 'start+') !== FALSE || strpos($value, '+end+') !== FALSE) {
      $dates = explode('+end+', $value);

      if (!empty($dates[0])) {
        // We have a start date.
        $start = str_replace('start+', '', $dates[0]);
      }
      if (!empty($dates[1])) {
        // We have an end date.
        $stop = $dates[1];
      }
    }
    else {
      $start = $value;
    }

    $dateTime = new DrupalDateTime();

    $dateInfo = date_parse($start);

    // @todo: Expand ranges if needed.
    if ($dateInfo['day']) {
      $day = ($dateInfo['day'] <= 9) ? '0' . $dateInfo['day'] : $dateInfo['day'];
      $month = ($dateInfo['month'] <= 9) ? '0' . $dateInfo['month'] : $dateInfo['month'];
      $date = $dateInfo['year'] . '-' . $month . '-' . $day . 'T00:00:00';

      $startDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $date);

      if (empty($stop)) {
        $stopDate = clone $startDate;
        $stopDate->setTimestamp(\Drupal::time()->getRequestTime());
      }
      else {
        $dateInfo = date_parse($stop);
        $day = ($dateInfo['day'] <= 9) ? '0' . $dateInfo['day'] : $dateInfo['day'];
        $month = ($dateInfo['month'] <= 9) ? '0' . $dateInfo['month'] : $dateInfo['month'];
        $date = $dateInfo['year'] . '-' . $month . '-' . $day . 'T00:00:00';
        $stopDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $date);
      }
    }
    else {
      $startDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $value);
      $stopDate = clone $startDate;
    }

    return [
      'start' => $startDate->format('U'),
      'stop' => $stopDate->format('U'),
    ];
  }

  /**
   * Calculates the result of the filter.
   *
   * @param int $value
   *   A unix timestamp.
   *
   * @return array
   *   An array with a start and end date as unix timestamps.
   */
  public function calculateResultFilter($value) {

    $date = new DrupalDateTime();
    $date->setTimestamp($value);
    $now = new DrupalDateTime();
    $now->setTimestamp(\Drupal::time()->getRequestTime());
    $interval = $date->diff($now);
    $future = $date > $now;

    $ranges = $this->getRange();

    $display = NULL;
    $raw = NULL;
    $range = [];

    // @todo: Expand ranges list w/ 'future' ranges and more granular ranges.
    if (!$future) {
      if (($interval->y === 0 && in_array($interval->m, range(0, 12)) && $interval->d <= 31) && in_array(static::FACETAPI_DATE_PAST_YEAR, $ranges)) {
        // Date within past year - the 365 days preceding today.
        $display = $this->t('Past Year');

        $now = new DrupalDateTime();
        $now->setTimestamp(\Drupal::time()->getRequestTime());
        $now->sub(new \DateInterval('P1Y'));
        $raw = $now->format('Y-m-d');
        $range = [self::FACETAPI_DATE_PAST_YEAR];
      }

      if (($interval->y === 0 && in_array($interval->m, range(0, 5))) && in_array(static::FACETAPI_DATE_PAST_6_MONTHS, $ranges)) {
        // Past 6 months - within the last 180 days.
        $display = $this->t('Past 6 Months');

        $now = new DrupalDateTime();
        $now->setTimestamp(\Drupal::time()->getRequestTime());
        $now->sub(new \DateInterval('P5M'));
        $raw = $now->format('Y-m-d');
        $range = [
          self::FACETAPI_DATE_PAST_6_MONTHS,
          self::FACETAPI_DATE_PAST_YEAR,
        ];
      }

      if (($interval->y === 0 && $interval->m === 0 && in_array($interval->d, range(0, 30))) && in_array(static::FACETAPI_DATE_PAST_MONTH, $ranges)) {
        // Past Month - within the last 30 days.
        $display = $this->t('Past Month');

        $now = new DrupalDateTime();
        $now->setTimestamp(\Drupal::time()->getRequestTime());
        $now->sub(new \DateInterval('P1M'));
        $raw = $now->format('Y-m-d');
        $range = [
          self::FACETAPI_DATE_PAST_MONTH,
          self::FACETAPI_DATE_PAST_6_MONTHS,
          self::FACETAPI_DATE_PAST_YEAR,
        ];
      }

      if (($interval->y === 0 && $interval->m === 0 && in_array($interval->d, range(0, 7))) && in_array(static::FACETAPI_DATE_PAST_WEEK, $ranges)) {
        // Past Week - within the last 7 days.
        $display = $this->t('Past Week');

        $now = new DrupalDateTime();
        $now->setTimestamp(\Drupal::time()->getRequestTime());
        $now->sub(new \DateInterval('P6D'));
        $raw = $now->format('Y-m-d');
        $range = [
          self::FACETAPI_DATE_PAST_WEEK,
          self::FACETAPI_DATE_PAST_MONTH,
          self::FACETAPI_DATE_PAST_6_MONTHS,
          self::FACETAPI_DATE_PAST_YEAR,
        ];
      }

      return [
        'display' => $display,
        'raw' => $raw,
        'range' => $range,
      ];
    }
  }

  /**
   * Retrieve configuration: Ranges to use.
   *
   * Default behaviour an integer for the steps that the facet works in.
   *
   * @return array
   *   The list of ranges for this config.
   */
  protected function getRange() {
    return $this->getConfiguration()['range'];
  }

}
