<?php

namespace Drupal\custom_search\Plugin\facets\query_type;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\QueryType\QueryTypePluginBase;
use Drupal\facets\Result\Result;
use Drupal\custom_search\ContentTypeMapInterface;
use Drupal\search_api\Query\QueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Support for CLIENT content Type facet within the Search API scope.
 *
 * Content Types are grouped according to mapping defined in
 * implemented interface ContentTypeMap.
 *
 * @FacetsQueryType(
 *   id = "custom_content_type_hierarchy",
 *   label = @Translation("CLIENT Content Type Hierarchy"),
 * )
 */
class CustomContentTypeHierarchy extends QueryTypePluginBase implements ContainerFactoryPluginInterface, ContentTypeMapInterface {

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $query = $this->query;

    // Only alter the query when there's an actual query object to alter.
    if (!empty($query)) {
      $operator = $this->facet->getQueryOperator();
      $field_identifier = $this->facet->getFieldIdentifier();
      $exclude = $this->facet->getExclude();

      if ($query->getProcessingLevel() === QueryInterface::PROCESSING_FULL) {
        // Set the options for the actual query.
        $options = &$query->getOptions();
        $options['search_api_facets'][$field_identifier] = $this->getFacetOptions();
      }

      // Add the filter to the query if there are active values.
      $active_items = $this->facet->getActiveItems();

      if (count($active_items)) {
        $filter = $query->createConditionGroup($operator, ['facet:' . $field_identifier]);
        foreach ($active_items as $value) {
          $filter->addCondition($this->facet->getFieldIdentifier(), $value, $exclude ? '<>' : '=');
        }
        $query->addConditionGroup($filter);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $query_operator = $this->facet->getQueryOperator();

    if (!empty($this->results)) {
      $facet_results = [];
      foreach ($this->results as $result) {
        if ($result['count'] || $query_operator == 'or') {
          $result_filter = $result['filter'];
          if ($result_filter[0] === '"') {
            $result_filter = substr($result_filter, 1);
          }
          if ($result_filter[strlen($result_filter) - 1] === '"') {
            $result_filter = substr($result_filter, 0, -1);
          }
          $count = $result['count'];
          $result = new Result($this->facet, $result_filter, $result_filter, $count);
          $facet_results[$result_filter] = $result;
        }
      }

      // Set active filter options.
      $active_filters = $this->facet->getActiveItems();

      // Restructure results according to desired hierarchy.
      // 3 levels deep max.
      // We don't need to sum up child results to parent results count
      // because of the method in which we index types for each entity.
      $results_hierarchy = [];

      foreach (self::CONTENT_TYPE_HIERARCHY as $type => $children) {

        $type_result = !empty($facet_results[$type]) ? $facet_results[$type] : NULL;;
        if ($type_result) {
          $results_hierarchy[$type] = $type_result;

          $this->setActive($type_result, $active_filters);
        }

        $subtype_results = [];
        foreach ($children as $subtypes => $subsubtypes) {
          $subtype_result = !empty($facet_results[$subtypes]) ? $facet_results[$subtypes] : NULL;

          if ($subtype_result) {
            $this->setActive($subtype_result, $active_filters);

            $subsubtype_results = [];

            foreach ($subsubtypes as $subsubtype => $children) {
              if ($facet_results[$subsubtype]) {
                $subsubtype_result = $facet_results[$subsubtype];
                $this->setActive($subsubtype_result, $active_filters);
                $subsubtype_results[] = $subsubtype_result;
              }
            }

            if ($subsubtype_results) {
              $subtype_result->setChildren($subsubtype_results);
            }

            $subtype_results[] = $subtype_result;
          }
        }

        if ($subtype_results) {
          $results_hierarchy[$type]->setChildren($subtype_results);
        }
      }

      $this->facet->setResults($results_hierarchy);
    }

    return $this->facet;

  }

  /**
   * Sets results to active given current filters.
   *
   * @param \Drupal\facets\Result\Result $result
   *   Facet Result.
   * @param array $active_filters
   *   Array of active filters.
   */
  protected function setActive(Result $result, array $active_filters = []) {
    if (in_array($result->getDisplayValue(), $active_filters)) {
      $result->setActiveState(TRUE);
    }
  }

}
