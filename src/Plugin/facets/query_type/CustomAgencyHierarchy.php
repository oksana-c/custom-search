<?php

namespace Drupal\custom_search\Plugin\facets\query_type;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\QueryType\QueryTypePluginBase;
use Drupal\facets\Result\Result;
use Drupal\search_api\Query\QueryInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Support for CLIENT Agency facets within the Search API scope.
 *
 * Agencies are grouped under 3 US Gov Branches.
 *
 * @FacetsQueryType(
 *   id = "custom_agency_hierarchy",
 *   label = @Translation("CLIENT Agency Hierarchy"),
 * )
 */
class CustomAgencyHierarchy extends QueryTypePluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $query = $this->query;

    // Only alter the query when there's an actual query object to alter.
    if (!empty($query)) {
      $operator = $this->facet->getQueryOperator();
      $field_identifier = $this->facet->getFieldIdentifier();
      $exclude = $this->facet->getExclude();

      if ($query->getProcessingLevel() === QueryInterface::PROCESSING_FULL) {
        // Set the options for the actual query.
        $options = &$query->getOptions();
        $options['search_api_facets'][$field_identifier] = $this->getFacetOptions();
      }

      // Add the filter to the query if there are active values.
      $active_items = $this->facet->getActiveItems();

      if (count($active_items)) {
        $filter = $query->createConditionGroup($operator, ['facet:' . $field_identifier]);
        foreach ($active_items as $value) {
          $filter->addCondition($this->facet->getFieldIdentifier(), $value, $exclude ? '<>' : '=');
        }
        $query->addConditionGroup($filter);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $query_operator = $this->facet->getQueryOperator();

    if (!empty($this->results)) {
      $facet_results = [];
      $branches = [];

      // Get TID and Label for US Gov Branch terms.
      $storage = $this->entityTypeManager->getStorage('taxonomy_term');
      // Not loading a full tree of term objects here to improve performance.
      $tree = $storage->loadTree('agency', 0, 1, FALSE);

      foreach ($tree as $term) {
        if (stripos($term->name, 'branch') !== FALSE) {
          $branches[$term->name] = $term->tid;
        }
      }

      $children = [];
      foreach ($this->results as $result) {
        if ($result['count'] || $query_operator == 'or') {
          $result_filter = $result['filter'];
          if ($result_filter[0] === '"') {
            $result_filter = substr($result_filter, 1);
          }
          if ($result_filter[strlen($result_filter) - 1] === '"') {
            $result_filter = substr($result_filter, 0, -1);
          }
          $count = $result['count'];
          $result = new Result($this->facet, $result_filter, $result_filter, $count);

          // Set active filter options.
          $active_filters = $this->facet->getActiveItems();
          if (in_array($result_filter, $active_filters)) {
            $result->setActiveState(TRUE);
          }

          $result_term = $storage->loadByProperties(['vid' => 'agency', 'name' => $result->getRawValue()]);
          $result_term = (is_array($result_term)) ? reset($result_term) : NULL;
          if ($result_term instanceof Term) {
            $result_term_parents = $storage->loadAllParents($result_term->id());
            if (!empty($result_term_parents)) {
              // Detect parent and add child under appropriate parent.
              unset($result_term_parents[$result_term->id()]);
              $parent_term = reset($result_term_parents);
              $parent_tid = $parent_term->id();
              if (in_array($parent_tid, $branches)) {
                $children[$parent_tid][] = $result;
              }
            }
          }
        }
      }

      foreach ($children as $tid => $items) {
        $result_label = trim(str_ireplace('branch', '', array_search($tid, $branches)));
        $parent_result = new Result($this->facet, $result_label, $result_label, 0);
        $parent_result->setChildren($items);
        $facet_results[] = $parent_result;
      }

      $this->facet->setResults($facet_results);
    }

    return $this->facet;
  }

}
