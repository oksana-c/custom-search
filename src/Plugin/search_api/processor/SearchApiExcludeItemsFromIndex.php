<?php

namespace Drupal\custom_search\Plugin\search_api\processor;

use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes entities marked as 'excluded' from being indexes.
 *
 * @SearchApiProcessor(
 *   id = "search_api_exclude_items_from_index",
 *   label = @Translation("Search API Exclude Items From Index - Custom Processor"),
 *   description = @Translation("Excludes inactive Staff Profiles from being indexed."),
 *   stages = {
 *     "alter_items" = -50
 *   }
 * )
 */
class SearchApiExcludeItemsFromIndex extends ProcessorPluginBase {

  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $object = $item->getOriginalObject()->getValue();
      $bundle = $object->bundle();

      // We need to be sure that the field actually exists on the bundle
      // before getting the value to avoid InvalidArgumentException
      // exceptions.
      // Remove Inactive Staff Profile from indexed items.
      if ($bundle == 'staff_profile' && $object->hasField('field_staff_active_flag')) {
        $value = $object->get('field_staff_active_flag')->getValue();
        if (!$value[0]['value']) {
          unset($items[$item_id]);
          continue;
        }
      }
    }
  }

}
