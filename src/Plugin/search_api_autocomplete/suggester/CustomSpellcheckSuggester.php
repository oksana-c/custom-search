<?php

namespace Drupal\custom_search\Plugin\search_api_autocomplete\suggester;

use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api_autocomplete\Plugin\search_api_autocomplete\suggester\Server;

/**
 * Provides a suggester plugin that retrieves suggestions from the server.
 *
 * The server needs to support the "search_api_autocomplete" feature for this to
 * work.
 *
 * Plugin returns suggestions; if no suggestion were found - returns spellcheck
 * suggestions.
 *
 * @SearchApiAutocompleteSuggester(
 *   id = "custom_spellcheck_suggester",
 *   label = @Translation("CLIENT Custom Suggester - Retrieve from server"),
 *   description = @Translation("Make suggestions based on the data indexed on the server. Includes spellcheck."),
 * )
 */
class CustomSpellcheckSuggester extends Server {

  /**
   * {@inheritdoc}
   */
  public function getAutocompleteSuggestions(QueryInterface $query, $incomplete_key, $user_input) {
    if (!($backend = static::getBackend($this->getSearch()->getIndex()))) {
      return [];
    }

    if ($this->configuration['fields']) {
      $query->setFulltextFields($this->configuration['fields']);
    }
    $suggestions = $backend->getAutocompleteSuggestions($query, $this->getSearch(), $incomplete_key, $user_input);

    if ($suggestions) {
      return $suggestions;
    }
    else {
      $spellcheck_suggestions = $backend->getSpellcheckSuggestions($query, $this->getSearch(), $incomplete_key,
        $user_input);
      return $spellcheck_suggestions;
    }
  }

}
