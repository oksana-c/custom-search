<?php

namespace Drupal\custom_search\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a custom sort facet block.
 *
 * Designed to work w/ Basic Search or Reports & Testimonies search.
 *
 * @Block(
 *   id = "sort_relevancy_date",
 *   admin_label = @Translation("Custom Sort by Relevancy and Date")
 * )
 */
class SortRelevancyDate extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Path Stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentPathStack $pathStack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pathStack = $pathStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.current')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Load parameters from URL.
    $base_path = $this->pathStack->getPath();
    $params = \Drupal::request()->query->all();
    if (empty($params)) {
      $params = \Drupal::request()->getRequestUri();
      $params = parse_url($params, PHP_URL_QUERY);
    }

    $sort_by = !empty($params['sort_by']) ? Html::escape($params['sort_by']) : NULL;
    $sort_order = !empty($params['sort_order']) ? Html::escape($params['sort_order']) : NULL;
    $keyword = !empty($params['keyword']) ? Html::escape($params['keyword']) : NULL;

    // Remove Sort parameters. Add them last.
    $trimmed_params = $params;
    if (isset($trimmed_params['sort_by'])) {
      unset($trimmed_params['sort_by']);
    }
    if (isset($trimmed_params['sort_order'])) {
      unset($trimmed_params['sort_order']);
    }

    // Sort widget options.
    $sortOptions = [
      'sort_by=search_api_relevance&sort_order=DESC' => $this->t('Sort by: Relevance'),
      'sort_by=docdate&sort_order=DESC' => $this->t('Sort by: Newest First'),
      'sort_by=docdate&sort_order=ASC' => $this->t('Sort by: Oldest First'),
    ];

    $option_base_uri = $base_path;
    $param_key = 0;

    foreach ($trimmed_params as $key => $value) {
      $param_glue = $param_key == 0 ? '?' : '&';

      if ($key === 'f') {
        foreach ($value as $index => $item) {
          $option_base_uri .= $param_glue . $key . '[' . $index . ']=' . $item;
          $param_key++;
          $param_glue = '&';
        }
      }
      else {
        $option_base_uri .= $param_glue . $key . '=' . htmlentities($value, ENT_QUOTES);
      }
      $param_key++;
    }

    $dropdown_options = '';
    $selected = FALSE;
    $selected_set = FALSE;

    foreach ($sortOptions as $value => $label) {

      if (stripos($value, (string) $sort_by) !== FALSE && stripos($value, (string) $sort_order) !== FALSE) {
        $selected = 'selected';
        $selected_set = TRUE;
      }

      // Set default option to Relevancy if no parameters are set
      // and keyword is used.
      if ($selected_set != TRUE && !empty($keyword) && $value == 'sort_by=search_api_relevance&sort_order=DESC') {
        $selected = 'selected';
        $selected_set = TRUE;
      }

      // Set default option if no parameters are set.
      if ($selected_set != TRUE && $value == 'sort_by=docdate&sort_order=DESC') {
        $selected = 'selected';
        $selected_set = TRUE;
      }

      if (stripos($option_base_uri, (string) 'search?') !== FALSE) {
        $dropdown_options .= '<option value="' . $option_base_uri . '&' . $value . '" ' . $selected . '>' . $label . '</option>';
      }
      else {
        $dropdown_options .= '<option value="' . $option_base_uri . '?' . $value . '" ' . $selected . '>' . $label . '</option>';
      }

      $selected = FALSE;
    }

    return [
      '#markup' => '<select id="search--sort-widget" title="' . $this->t('Sort by') . '">' . $dropdown_options . '</select>',
      '#allowed_tags' => [
        'select',
        'option',
      ],
      '#attached' => [
        'library' => 'custom_search/custom_search_sort',
      ],
      '#cache' => [
        'contexts' => [
          'url.query_args',
        ],
      ],
    ];
  }

}
