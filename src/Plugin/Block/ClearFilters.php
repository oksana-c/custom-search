<?php

namespace Drupal\custom_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a date facet block.
 *
 * @Block(
 *   id = "clear_filters",
 *   admin_label = @Translation("Clear Filters")
 * )
 */
class ClearFilters extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return custom_search_clear_filter_url();
  }

}
