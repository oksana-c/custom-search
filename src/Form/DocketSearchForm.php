<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class DocketSearchForm extends CustomSearchFilter {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'docket_search_form';
  }

  /**
   * Array of fields.
   *
   * @var array
   */
  protected $fields = [
    'file',
    'protestor',
    'solicitation',
    'agency',
    'subagency',
  ];

  /**
   * View for form to update.
   *
   * @var string
   */
  protected $facetSourceId = 'search_api:views_block__docket_search__block_1';

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queries = \Drupal::request()->query->all();
    $agency = $form_state->getValue('agency') != NULL ? $form_state->getValue('agency') : ((!empty($queries['agency'])) ? $queries['agency'] : NULL);
    $subagency = $form_state->getValue('subagency') != NULL ? $form_state->getValue('subagency') : ((!empty($queries['subagency'])) ? $queries['subagency'] : NULL);
    $agencies = $this->buildAgencyList('agency_term_top_level', 'bid_protest_docket');
    $subagencies = is_null($agency) ? [] : $this->buildSubAgencyList($agency, 'subagency_or_agency_term', 'bid_protest_docket');

    $form['accessible_link'] = [
      "#markup" => '<div class="visually-hidden"><a href="#s-skipLinkTargetForMainSearchResults">' . $this->t("Skip to main search results") . '</a></div><div id="s-skipLinkTargetForFilterOptions" tabindex="-1"></div>',
    ];
    $form['closed'] = [
      '#type' => 'radios',
      '#title' => $this->t('Docket Status'),
      '#options' => [
        'all' => $this->t('All'),
        'open' => $this->t('Open'),
        'closed' => $this->t('Closed'),
      ],
      '#default_value' => 'all',
    ];
    $form['protestor'] = [
      '#title' => 'Protester',
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter Protestor'),
        'title' => $this->t('Enter Protestor'),
      ],
      '#required' => FALSE,
    ];
    $form['solicitation'] = [
      '#title' => $this->t('Solicitation'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter Solicitation Number'),
        'title' => $this->t('Enter Solicitation Number'),
      ],
      '#required' => FALSE,
    ];
    $form['file'] = [
      '#title' => $this->t('File'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter File Number'),
        'title' => $this->t('Enter File Number'),
      ],
      '#required' => FALSE,
    ];
    $form['agency'] = [
      '#type' => 'select',
      '#size' => NULL,
      '#default_value' => 'all',
      '#options' => $agencies,
      '#title' => $this->t('Agency'),
      '#ajax' => [
        'callback' => '::subagencyAjaxCallback',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'federal-docket-form',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];
    $form['subagency'] = $this->subagencyForm($agency, $subagency, $subagencies);
    if (isset($queries['closed'])) {
      if ($queries['closed'] == 0) {
        $form['closed']['#default_value'] = 'open';
      }
      elseif ($queries['closed'] == 1) {
        $form['closed']['#default_value'] = 'closed';
      }
    }
    foreach ($this->fields as $field) {
      if (!empty($queries[$field])) {
        $form[$field]['#default_value'] = $queries[$field];
      }
    }
    $form['footer'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'form-footer',
      ],
    ];
    $form['footer']['clear_filters'] = custom_search_clear_filter_url(['fragment' => 's-skipLinkTargetForFilterOptions']);
    $form['footer']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search the Docket'),
      '#weight' => 10,
    ];
    $form['#attributes']['id'] = 'federal-docket-form';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];
    foreach ($this->fields as $field) {
      if ($q = $form_state->getValue($field)) {
        $query[$field] = $q;
      }
    }
    if ($q = $form_state->getValue('closed')) {
      if ($q == 'closed') {
        $query['closed'] = 1;
      }
      elseif ($q == 'open') {
        $query['closed'] = 0;
      }
    }

    $url = Url::fromRoute('<current>', [], ['query' => $query, 'fragment' => 's-skipLinkTargetForMainSearchResults']);
    $form_state->setRedirectUrl($url);
    return FALSE;
  }

}
