<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class OpenRecsSearchForm extends CustomSearchFilter {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'open_recs_search_form';
  }

  /**
   * Array of fields.
   *
   * @var array
   */
  protected $fields = [
    'priority',
    'keyword',
    'topic',
    'subject',
    'agency',
    'subagency',
  ];

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queries = \Drupal::request()->query->all();
    $topics = $this->buildAgencyList('related_product_topic_name', 'recommendations');
    $agency = $form_state->getValue('agency') != NULL ? $form_state->getValue('agency') : ((!empty($queries['agency'])) ? $queries['agency'] : NULL);
    $subagency = $form_state->getValue('subagency') != NULL ? $form_state->getValue('subagency') : ((!empty($queries['subagency'])) ? $queries['subagency'] : NULL);
    $agencies = $this->buildAgencyList('agency_term_top_level', 'recommendations');
    $subagencies = is_null($agency) ? [] : $this->buildSubAgencyList($agency, 'rec_subagency_term', 'recommendations');

    $form['accessible_link'] = [
      "#markup" => '<div class="visually-hidden"><a href="#s-skipLinkTargetForMainSearchResults">' . $this->t("Skip to main search results") . '</a></div><div id="s-skipLinkTargetForFilterOptions" tabindex="-1"></div>',
    ];
    $form['priority'] = [
      '#type' => 'radios',
      '#title' => $this->t('Search By'),
      '#options' => [
        '1' => $this->t('Priority Recommendations'),
        'all' => $this->t('All Recomendations'),
      ],
      '#default_value' => '1',
    ];

    $form['keyword'] = [
      '#type' => 'search_api_autocomplete',
      '#search_id' => 'autocomplete_product_open_recs',
      '#attributes' => [
        'placeholder' => $this->t('Enter Keyword or Phrase'),
        'title' => $this->t('Search'),
      ],
      '#title' => $this->t('Search'),
    ];
    $form['topic'] = [
      '#type' => 'select',
      '#size' => NULL,
      '#default_value' => "all",
      '#options' => $topics,
      '#title' => $this->t('Search By Topic'),
    ];
    $form['subject'] = [
      '#type' => 'search_api_autocomplete',
      '#search_id' => 'autocomplete_product_subject_terms',
      '#attributes' => [
        'placeholder' => $this->t('Enter Term'),
        'title' => $this->t('Search'),
      ],
      '#title' => $this->t('Subject Term'),
    ];
    $form['agency'] = [
      '#type' => 'select',
      '#size' => NULL,
      '#default_value' => 'all',
      '#options' => $agencies,
      '#title' => $this->t('Agency'),
      '#ajax' => [
        'callback' => '::subagencyAjaxCallback',
        'event' => 'change',
        'speed' => 'fast',
        'options' => [
          'query' => [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE],
        ],
        'wrapper' => 'open-recs-search-form',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];
    $form['subagency'] = $this->subagencyForm($agency, $subagency, $subagencies);
    foreach ($this->fields as $field) {
      if (!empty($queries[$field])) {
        $form[$field]['#default_value'] = $queries[$field];
      }
    }
    $form['clear_filters'] = custom_search_clear_filter_url(['fragment' => 's-skipLinkTargetForMainSearchResults']);
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];
    $form['#attributes']['id'] = 'open-recs-search-form';
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];
    foreach ($this->fields as $field) {
      if ($q = $form_state->getValue($field)) {
        $query[$field] = $q;
      }
    }
    $url = Url::fromRoute('<current>', [], ['query' => $query, 'fragment' => 's-skipLinkTargetForMainSearchResults']);
    $form_state->setRedirectUrl($url);
    return FALSE;
  }

}
