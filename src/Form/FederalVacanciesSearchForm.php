<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class FederalVacanciesSearchForm extends CustomSearchFilter {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vacancies_search_form';
  }

  /**
   * Array of string fields.
   *
   * @var array
   */
  protected $fields = [
    'title',
    'nominee',
    'acting_official',
    'agency',
    'subagency',
    'vacancy_status',
  ];

  /**
   * View for form to update.
   *
   * @var string
   */
  protected $facetSourceId = 'search_api:views_block__federal_vacancies__block_1';

  /**
   * Builds vacancy status list for select list.
   */
  public function buildVacancyStatusList() {
    $cacheId = 'vacancy_status';
    if ($cache = $this->cacheBackend->get($cacheId)) {
      return $cache->data;
    }
    else {
      $list = $this->storage->loadTree('federal_vacancy_status', 0, 1);
      $items = [];
      foreach ($list as $item) {
        $items[$item->name] = $item->name;
      }
      $items = ['all' => $this->t("All")] + $items;
      $this->cacheBackend->set($cacheId, $items);
      return $items;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $queries = \Drupal::request()->query->all();
    $agency = $form_state->getValue('agency') != NULL ? $form_state->getValue('agency') : ((!empty($queries['agency'])) ? $queries['agency'] : NULL);
    $subagency = $form_state->getValue('subagency') != NULL ? $form_state->getValue('subagency') : ((!empty($queries['subagency'])) ? $queries['subagency'] : NULL);
    $agencies = $this->buildAgencyList('agency_term_top_level', 'federal_vacancies');
    $subagencies = is_null($agency) ? [] : $this->buildSubAgencyList($agency, 'subagency_or_agency_term', 'federal_vacancies');
    $vacancies = $this->buildVacancyStatusList();

    $form['accessible_link'] = [
      "#markup" => '<div class="visually-hidden"><a href="#s-skipLinkTargetForMainSearchResults">' . $this->t("Skip to main search results") . '</a></div><div id="s-skipLinkTargetForFilterOptions" tabindex="-1"></div>',
    ];
    $form['title'] = [
      '#title' => $this->t('Position Title'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter Position Title'),
        'title' => $this->t('Enter Position Title'),
      ],
      '#required' => FALSE,
    ];
    $form['acting_official'] = [
      '#title' => $this->t('Acting Official'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter Acting Official'),
        'title' => $this->t('Enter Acting Official'),
      ],
      '#required' => FALSE,
    ];
    $form['nominee'] = [
      '#title' => $this->t('Nominee'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter Nominee'),
        'title' => $this->t('Enter Nominee'),
      ],
      '#required' => FALSE,
    ];
    $form['vacancy_status'] = [
      '#type' => 'select',
      '#size' => NULL,
      '#default_value' => 'all',
      '#options' => $vacancies,
      '#title' => $this->t('Vacancy Status'),
    ];
    foreach ($this->fields as $field) {
      if (!empty($queries[$field])) {
        $form[$field]['#default_value'] = $queries[$field];
      }
    }
    $form['agency'] = [
      '#type' => 'select',
      '#size' => NULL,
      '#default_value' => 'all',
      '#options' => $agencies,
      '#title' => $this->t('Agency'),
      '#ajax' => [
        'callback' => '::subagencyAjaxCallback',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'federal-vacancies-form',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];
    $form['subagency'] = $this->subagencyForm($agency, $subagency, $subagencies);
    if (isset($queries['agency'])) {
      $form['agency']['#default_value'] = $agency;
    }
    $form['footer'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'form-footer',
      ],
    ];
    $form['footer']['suffix'] = [
      '#weight' => 11,
    ];
    $form['footer']['clear_filters'] = custom_search_clear_filter_url(['fragment' => 's-skipLinkTargetForFilterOptions']);
    $form['footer']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search Vacancies'),
      '#weight' => 10,
    ];
    $form['#attributes']['id'] = 'federal-vacancies-form';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];
    foreach ($this->fields as $field) {
      if ($q = $form_state->getValue($field)) {
        $query[$field] = $q;
      }
    }
    $url = Url::fromRoute('<current>', [], ['query' => $query, 'fragment' => 's-skipLinkTargetForMainSearchResults']);
    $form_state->setRedirectUrl($url);
    return FALSE;
  }

}
