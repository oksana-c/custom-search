<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class DatabaseRulesSearchForm extends CustomSearchFilter {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'database_rules_search_form';
  }

  /**
   * Array of string fields.
   *
   * @var array
   */
  protected $fields = [
    'title',
    'type',
    'identifier',
    'effective_start_date',
    'effective_end_date',
    'received_start_date',
    'received_end_date',
    'priority',
    'agency',
    'subagency',
  ];

  /**
   * View for form to update.
   *
   * @var string
   */
  protected $facetSourceId = 'search_api:views_block__database_of_rules__block_1';

  /**
   * Builds vacancy status list for select list.
   */
  public function buildPrioritiesList() {
    $cacheId = 'federal_rules_priority';
    if ($cache = $this->cacheBackend->get($cacheId)) {
      return $cache->data;
    }
    else {
      $list = $this->storage->loadTree('federal_rules_priority', 0, 1);
      $items = [];
      foreach ($list as $item) {
        $items[$item->name] = $item->name;
      }
      $items = ['all' => $this->t('All')] + $items;
      $this->cacheBackend->set($cacheId, $items);
      return $items;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $queries = \Drupal::request()->query->all();
    $agency = $form_state->getValue('agency') != NULL ? $form_state->getValue('agency') : ((!empty($queries['agency'])) ? $queries['agency'] : NULL);
    $subagency = $form_state->getValue('subagency') != NULL ? $form_state->getValue('subagency') : ((!empty($queries['subagency'])) ? $queries['subagency'] : NULL);
    $agencies = $this->buildAgencyList('agency_term_top_level', 'federal_rules');
    $subagencies = is_null($agency) ? [] : $this->buildSubAgencyList($agency, 'subagency_or_agency_term', 'federal_rules');

    $form['accessible_link'] = [
      "#markup" => '<div class="visually-hidden"><a href="#s-skipLinkTargetForMainSearchResults">' . $this->t("Skip to main search results") . '</a></div><div id="s-skipLinkTargetForFilterOptions" tabindex="-1"></div>',
    ];
    $form['description'] = [
      '#markup' => '<p>' . $this->t('We track all rules (major and non-major) submitted to us. Your search results will include all rules submitted to us..') . '</p>',
    ];
    $form['title'] = [
      '#title' => $this->t('Title'),
      '#type' => 'search_api_autocomplete',
      '#search_id' => 'autocomplete_dbor_title',
      '#attributes' => [
        'placeholder' => $this->t('Enter Title'),
      ],
    ];
    $form['identifier'] = [
      '#title' => $this->t('Identifier'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => '',
      '#attributes' => [
        'placeholder' => $this->t('Enter Identifier'),
      ],
      '#required' => FALSE,
    ];
    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Rule Type'),
      '#options' => [
        'all' => $this->t('All'),
        'Major' => $this->t('Major'),
        'Non-Major' => $this->t('Non-Major'),
      ],
      '#default_value' => 'all',
    ];
    $form['priority'] = [
      '#type' => 'radios',
      '#title' => $this->t('Rule Priority'),
      '#options' => [
        'all' => $this->t('All'),
        'Significant/Substantive' => $this->t('Significant/Substantive'),
        'Routine/Info/Other' => $this->t('Other'),
      ],
      '#default_value' => 'all',
    ];
    $form['effective_start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Rule Effective Date'),
      '#required' => FALSE,
    ];
    $form['effective_end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('To'),
      '#required' => FALSE,
    ];
    $form['received_start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Date Received by CLIENT'),
      '#required' => FALSE,
    ];
    $form['received_end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('To'),
      '#required' => FALSE,
    ];
    $form['agency'] = [
      '#type' => 'select',
      '#size' => NULL,
      '#default_value' => 'all',
      '#options' => $agencies,
      '#title' => $this->t('Agency'),
      '#ajax' => [
        'callback' => '::subagencyAjaxCallback',
        'event' => 'change',
        'speed' => 'fast',
        'options' => [
          'query' => [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE],
        ],
        'wrapper' => 'database-search-form',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];
    $form['subagency'] = $this->subagencyForm($agency, $subagency, $subagencies);
    if (isset($queries['agency'])) {
      $form['agency']['#default_value'] = $agency;
    }
    foreach ($this->fields as $field) {
      if (!empty($queries[$field])) {
        $form[$field]['#default_value'] = $queries[$field];
      }
    }
    $form['footer'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'form-footer',
      ],
    ];
    $form['footer']['suffix'] = [
      '#markup' => '<div id="s-skipLinkTargetForMainSearchResults" tabindex="-1"></div>',
      '#weight' => 11,
    ];
    $form['footer']['clear_filters'] = custom_search_clear_filter_url(['fragment' => 's-skipLinkTargetForFilterOptions']);
    $form['footer']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#weight' => 10,
    ];
    $form['#attributes']['id'] = 'database-search-form';
    $form['#cache']['contexts'][] = 'url.query_args';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];
    foreach ($this->fields as $field) {
      if ($q = $form_state->getValue($field)) {
        $query[$field] = $q;
      }
    }
    $url = Url::fromRoute('<current>', [], ['query' => $query, 'fragment' => 's-skipLinkTargetForMainSearchResults']);
    $form_state->setRedirectUrl($url);
    return FALSE;
  }

}
