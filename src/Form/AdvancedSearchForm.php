<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Implements the Advanced Search form controller.
 *
 * This form is using ajax callbacks to add condition fieldsets.
 *
 * @see \Drupal\Core\Form\FormBase
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class AdvancedSearchForm extends FormBase {

  /**
   * Form with 'add more' and 'remove' buttons.
   *
   * This example shows a button to "add more" - add another textfield, and
   * the corresponding "remove" button.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Gather the number of conditions present in the form.
    // Default number of fields = 3.
    $condition_number = $form_state->get('condition_number') ? $form_state->get('condition_number') : 3;
    // Allow max of 10 conditions.
    $condition_number = ($condition_number >= 10) ? 10 : $condition_number;
    // If "Remove" button was clicked, get the condition number
    // that must be removed.
    $condition_number_remove = $form_state->get('condition_number_remove');

    $submitted_values = $form_state->cleanValues()->getValues();
    $condition_index = (isset($submitted_values['condition_fieldset']) && is_array($submitted_values['condition_fieldset'])) ? $this->getFieldsetIndex($submitted_values['condition_fieldset']) : [1];

    // We have to ensure that there is at least one condition fieldset.
    if ($condition_number === NULL) {
      $form_state->set('condition_number', 1);
      $condition_number = 1;
    }

    $condition_index_count = count($condition_index);

    // If we need to add fieldset, we add them to $condition_index first,
    // then iterate over $condition_index to build the form.
    if ($condition_index_count < $condition_number) {
      $diff = $condition_number - $condition_index_count;
      $max = max($condition_index);

      if (!is_null($max)) {
        for ($x = 1; $x <= $diff; $x++) {
          $condition_index[] = $max + $x;
        }
      }
    }

    $form['#tree'] = TRUE;
    $form['search_link'] = [
      '#type' => 'markup',
      '#markup' => '<div class="basic-search-link"><a href="/search">' . $this->t('Perform Basic Search') . '</a></div>',
    ];
    $form['condition_fieldset'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="condition-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    foreach ($condition_index as $i) {
      if ($i !== $condition_number_remove) {

        $form['condition_fieldset'][$i]['condition'] = [
          '#type' => 'select',
          '#default_value' => 'all',
          '#options' => [
            'AND' => $this->t('Must Include'),
            'NOT' => $this->t('Must NOT Include'),
            'OR' => $this->t('May Include'),
          ],
          '#attributes' => [
            'title' => $this->t('Conditions'),
          ],
        ];
        $form['condition_fieldset'][$i]['keyword'] = [
          '#type' => 'textfield',
          '#attributes' => [
            'placeholder' => $this->t('Keywords'),
            'title' => $this->t('Keywords'),
          ],
        ];
        // If there is more than one condition, add the remove button.
        if ($i > 1) {
          $form['condition_fieldset'][$i]['actions']['remove_condition'] = [
            '#type' => 'submit',
            '#value' => '-',
            '#options' => [
              'condition_number' => $i,

            ],
            '#attributes' => [
              'class' => ['remove'],
              'title' => $this->t('Remove Keywords'),
            ],
            '#submit' => [
              '::removeCallback',
            ],
            '#ajax' => [
              'callback' => '::addmoreCallback',
              'wrapper' => 'condition-fieldset-wrapper',
            ],
          ];
        }
      }
    }

    $form['condition_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    if ($condition_number < 10) {
      $form['condition_fieldset']['actions']['add_condition'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add a Line'),
        '#submit' => [
          '::addOne',
        ],
        '#ajax' => [
          'callback' => '::addMoreCallback',
          'wrapper' => 'condition-fieldset-wrapper',
        ],
      ];
    }

    // Date fieldset.
    $form['prefix'] = [
      '#type' => 'markup',
      '#markup' => '<div class="date-range">',
    ];
    $form['date_label'] = [
      '#type' => 'markup',
      '#markup' => '<div class="label">' . $this->t('Narrow by Date:') . '</div>',
    ];
    $form['start'] = [
      '#type' => 'date',
      '#title' => t('Start'),
      '#required' => FALSE,
    ];
    $form['end'] = [
      '#type' => 'date',
      '#title' => t('End'),
      '#required' => FALSE,
    ];
    $form['suffix'] = [
      '#type' => 'markup',
      '#markup' => '</div>',
      '#weight' => 100,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    // If the "Remove" button was used, then remove fieldset number
    // from condition index, so that we can set updated
    // condition_number value.
    if (($key = array_search($condition_number_remove, $condition_index)) !== FALSE) {
      unset($condition_index[$key]);
    }

    // Reset condition number based on amount of form fields.
    $form_state->set('condition_number', count($condition_index));
    // Reset condition_number_remove since it should only be set
    // by removeCallback() and not persist.
    $form_state->set('condition_number_remove', NULL);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advanced_search_form';
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['condition_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $condition_field = $form_state->get('condition_number');
    $add_button = $condition_field + 1;
    $form_state->set('condition_number', $add_button);

    // Since our buildForm() method relies on the value of 'condition_number' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $condition_number_remove = $trigger['#options']['condition_number'];
    // Set the condition number to be removed.
    $form_state->set('condition_number_remove', $condition_number_remove);
    // Set resulting condition_number.
    $form_state->set('condition_number', $form_state->get('condition_number') - 1);

    // Since our buildForm() method relies on the value of 'condition_number' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Get key index of condition fieldsets.
   *
   * @param array $array
   *   Array of form elements.
   *
   * @return array
   *   Count of fieldsets.
   */
  public function getFieldsetIndex(array $array) {
    // Set a number that equals to the greatest fieldset index.
    $index = [];
    foreach ($array as $key => $value) {
      if (array_key_exists('condition', $value) && array_key_exists('keyword', $value)) {
        $index[] = $key;
      }
    }

    return $index;
  }

  /**
   * Final submit handler.
   *
   * Reports what values were finally set.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $redirect_url = NULL;
    $query['keyword'] = '';
    $values = $form_state->cleanValues()->getValues();
    $conditions = [];
    $sorted_conditions = [];
    $keyword_query = NULL;

    foreach ($values['condition_fieldset'] as $key => $value) {
      if (is_numeric($key) && array_key_exists('condition', $value) && array_key_exists('keyword', $value)) {
        $conditions[$value['condition']][] = $value['keyword'];
      }
    }

    $sorted_conditions['AND'] = !empty($conditions['AND']) ? array_filter($conditions['AND']) : NULL;
    $sorted_conditions['OR'] = !empty($conditions['OR']) ? array_filter($conditions['OR']) : NULL;
    $sorted_conditions['NOT'] = !empty($conditions['NOT']) ? array_filter($conditions['NOT']) : NULL;
    $sorted_conditions = array_filter($sorted_conditions);

    // Variety of operators.
    // Possible queries:
    // NOT "naval shipyards"
    // NOT "naval" NOT "studies"
    // May contain multiple negations.
    // "climate" AND NOT "naval" NOT "studies".
    // "southwestern" AND "climate" AND NOT "naval" NOT "studies"
    // "climate" AND NOT "migration"
    // "southwestern" OR "climate" AND NOT "naval" NOT "studies"
    // "southwestern" OR "climate" AND "federal" AND NOT "naval" NOT "studies".
    foreach ($sorted_conditions as $operator => $condition_collection) {
      foreach ($condition_collection as $key => $condition) {
        if ($key > 0 || !empty($keyword_query)) {
          // Add space in between negations.
          $keyword_query .= ' ';
        }

        if ($operator === 'NOT' && !empty($keyword_query) && $key === 0) {
          // If negation operator is used in conjunction w/ other
          // operators, we need to prepend the first negation w/ 'AND'.
          $keyword_query .= 'AND ';
        }

        if (in_array($operator, ['AND', 'OR']) && empty($keyword_query)) {
          $keyword_query .= '"' . trim($condition) . '"';
        }
        else {
          $keyword_query .= $operator . ' "' . trim($condition) . '"';
        }
      }
    }

    // Add Date range query params.
    // If start is empty - do not add dates to the query.
    if ($values['start']) {
      $start = !empty($values['start']) ? $values['start'] : NULL;
      $end = !empty($values['end']) ? '+end+' . $values['end'] : NULL;
      $query['f[0]'] = 'date:start+' . $start . $end;
    }

    if ($keyword_query) {
      $query['keyword'] = $keyword_query;
    }

    $url = Url::fromRoute('view.search.search', [], ['query' => $query, 'fragment' => 's-skipLinkTargetForMainSearchResults']);
    $form_state->setRedirectUrl($url);

    return FALSE;
  }

}
