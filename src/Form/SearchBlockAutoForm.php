<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * {@inheritdoc}
 */
class SearchBlockAutoForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'search_block_auto_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['search'] = [
      '#type' => 'search_api_autocomplete',
      '#search_id' => 'autocomplete_site_wide_search_block',
      '#attributes' => [
        'placeholder' => $this->t('Search CLIENT.gov'),
        'title' => $this->t('Search CLIENT.gov'),
        'class' => ['form-control'],
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#attributes' => [
        'id' => 'edit-submit-site-wide-search',
        'class' => [
          'btn',
          'btn-primary',
        ],
      ],
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($term = $form_state->getValue('search')) {
      $query['keyword'] = $term;
    }
    $url = Url::fromRoute('view.search.search', [], ['query' => $query]);
    $form_state->setRedirectUrl($url);
    return FALSE;
  }

}
