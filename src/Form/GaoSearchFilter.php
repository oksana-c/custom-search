<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\search_api\Entity\Index;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class CustomSearchFilter extends FormBase {

  /**
   * The cache.default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * {@inheritdoc}
   */
  public function __construct(DefaultFacetManager $facet_manager, EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cache_backend) {
    $this->facetManager = $facet_manager;
    $this->facetStorage = $entity_type_manager->getStorage('facets_facet');
    $this->storage = $entity_type_manager->getStorage('taxonomy_term');
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('facets.manager'),
      $container->get('entity_type.manager'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_search_filter';
  }

  /**
   * Filters tids from result list.
   */
  public function getTidsFromResults($results, $field) {
    foreach ($results as $item) {
      $orig = $item->getOriginalObject();
      $entity = $orig->getValue();
      $term_targets = $entity->get($field)->getValue();
      foreach ($term_targets as $term) {
        $tids[] = $term['target_id'];
      }
    }
    return $tids;
  }

  /**
   * Loads Search API index.
   */
  public function loadIndex() {
    if (empty($this->index)) {
      $this->index = Index::load('default_solr_index');
    }
    return $this->index;
  }

  /**
   * Creates list of term children.
   */
  private function getChildren($tid) {
    $children = $this->storage->loadTree('agency', $tid);
    $tree = [];
    foreach ($children as $child) {
      $tree[] = $child->name;
    }
    return $tree;
  }

  /**
   * Creates subagency form item.
   */
  public function subagencyForm($agency, $subagency, $subagencies) {
    if ($agency == '' || $agency == 'all') {
      return [
        '#markup' => "",
      ];
    }
    else {
      if (empty($subagencies)) {
        return [
          '#type' => 'markup',
          '#markup' => $this->t("No Sub Agencies found from the selected query."),
          '#prefix' => '<div id="edit-subagency">',
          '#suffix' => '</div>',
        ];
      }
      else {
        return [
          '#type' => 'select',
          '#size' => NULL,
          '#default_value' => $subagency,
          '#validated' => TRUE,
          '#options' => ["all" => $this->t("All")] + $subagencies,
          '#title' => $this->t('Sub Agency'),
        ];
      }
    }
  }

  /**
   * Builds agency list for select list.
   */
  public function buildAgencyList($field, $type) {
    $cacheId = 'custom_search_agency_list__' . $field . '_' . $type;
    if ($cache = $this->cacheBackend->get($cacheId)) {
      return $cache->data;
    }
    else {
      $list = $this->getFacetResults($field, $type);
      $list = ['all' => $this->t("All")] + $list;
      $this->cacheBackend->set($cacheId, $list);
      return $list;
    }
  }

  /**
   * Builds subagency list for select list.
   */
  public function buildSubagencyList($agency, $field, $type) {
    $cacheId = 'custom_search_subagency_list__' . $type . '__' . strtolower(str_replace(' ', '_', $agency));

    if ($cache = $this->cacheBackend->get($cacheId)) {
      return $cache->data;
    }
    else {
      $agencyId = $this->getTidByName($agency);
      $children = $this->storage->loadTree('agency', $agencyId);
      $list = $this->getFacetResults($field, $type);
      $subagencies = [];
      $treeTerms = [];
      foreach ($children as $child) {
        $treeTerms[$child->name] = $child->name;
      }
      foreach ($list as $item) {
        if (isset($treeTerms[$item])) {
          $subagencies[$item] = $item;
        }
      }
      ksort($subagencies);
      $this->cacheBackend->set($cacheId, $subagencies);
      return $subagencies;
    }
  }

  /**
   * Returns list of unique items from solr from a specific field.
   */
  public function getFacetResults($field, $type) {
    $results = [];
    $index = $this->loadIndex();
    $parse_mode = \Drupal::service('plugin.manager.search_api.parse_mode')
      ->createInstance('direct');
    $query = $index->query();
    // Set results to 0. We still get facet info but not documents.
    $query->range(0, 0);
    $query->setParseMode($parse_mode);
    $query->setOption('search_api_facets', [
      $field => [
        'field' => $field,
        'limit' => 0,
        'operator' => 'AND',
        'min_count' => 1,
        'missing' => TRUE,
      ],
    ]);
    // Set additional conditions.
    $query->addCondition('type', $type);
    $searchResults = $query->execute();
    $extraData = $searchResults->getExtraData('search_api_facets');
    if (isset($extraData[$field])) {
      foreach ($extraData[$field] as $result) {
        if ($result['filter'] != '!') {
          $results[trim($result['filter'], '"')] = trim($result['filter'], '"');
        }
      }
    }
    return $results;
  }

  /**
   * Builds a taxo term list for select list.
   */
  public function buildTermList($facetSourceId, $facetName, $queries) {
    $list = [];
    $cacheId = $facetName . implode('_', $queries);
    if ($cache = $this->cacheBackend->get($cacheId)) {
      return $cache->data;
    }
    else {
      $facets = $this->facetManager->getFacetsByFacetSourceId($facetSourceId);
      foreach ($facets as $facet) {
        $id = $facet->getOriginalId();
        if ($id == $facetName) {
          $list = [];
          $facet = $this->facetManager->returnProcessedFacet($facet);
          $results = $facet->getResults();
          foreach ($results as $result) {
            $list[$result->getDisplayValue()] = $result->getDisplayValue();
          }
        }
      }
      $list = ['all' => $this->t("All")] + $list;
      $this->cacheBackend->set($cacheId, $list);
      return $list;
    }
  }

  /**
   * Gets the term id from the name.
   */
  public function getTidbyName($title, $vocab = 'agency') {
    $query = $this->storage->getQuery()->condition('name', $title)->condition('vid', $vocab);
    if ($query) {
      $ids = $query->execute();
      return array_pop($ids);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Gets the term name from the id.
   */
  public function getNamebyTid($tid) {
    $term = $this->storage->load($tid);
    if ($term) {
      return $term->getName();
    }
    else {
      return FALSE;
    }
  }

  /**
   * Callback for subagency select list.
   */
  public function subagencyAjaxCallback(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
