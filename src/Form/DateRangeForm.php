<?php

namespace Drupal\custom_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class DateRangeForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'date_range_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Load values from URL.
    $params = \Drupal::request()->query->all();
    if (empty($params)) {
      $params = \Drupal::request()->getRequestUri();
      $params = parse_url($params, PHP_URL_QUERY);
    }

    $startDate = $form_state->getValue('start') ? $form_state->getValue('start') : NULL;
    $endDate = $form_state->getValue('end') ? $form_state->getValue('end') : NULL;

    if (!empty($params) && (!$startDate || !$endDate)) {
      foreach ($params as $key => $param) {
        if ($key == 'f') {
          // Targeting facets only.
          foreach ($param as $value) {
            if (strpos($value, 'start+') !== FALSE || strpos($value, '+end+') !== FALSE) {
              $dates = explode('+end+', $value);
              $endDate = (!empty($dates[1])) ? $dates[1] : $endDate;
              $start = explode('start+', $dates[0]);
              $startDate = (!empty($start[1])) ? $start[1] : $startDate;
            }

          }
        }
      }
    }

    $urlClear = $this->getClearDatesUrl();

    $form['heading'] = [
      '#type' => 'markup',
      '#markup' => '<h4>' . $this->t('Custom Date Range') . '</h4>',
    ];
    $form['prefix'] = [
      '#type' => 'markup',
      '#markup' => '<div class="date-facet--form">',
    ];
    $form['start'] = [
      '#type' => 'date',
      '#title' => t('Start'),
      '#required' => TRUE,
      '#default_value' => $startDate,
    ];
    $form['end'] = [
      '#type' => 'date',
      '#title' => t('End'),
      '#required' => FALSE,
      '#default_value' => $endDate,
    ];
    $form['clear'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<div><a href="@url" title="Search by any date" id="custom-date-range--clear">Clear Dates</a></div>', ['@url' => $urlClear]),
      '#weight' => 100,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go'),
      '#button_type' => 'primary',
    ];
    $form['clear'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<div><a href="@url" title="Search by any date" id="custom-date-range--clear">Clear dates</a></div>',
        ['@url' => $urlClear]),
      '#weight' => 100,
    ];
    $form['suffix'] = [
      '#type' => 'markup',
      '#markup' => '</div>',
      '#weight' => 100,
    ];
    $form['#cache']['contexts'][] = 'url.query_args';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query_string = \Drupal::request()->getQueryString();
    $query_string = urldecode($query_string);

    $start = $form_state->getValue('start');

    if ($form_state->getValue('start')) {
      $query_params = explode('&', $query_string);

      $date_param_exists = FALSE;
      $date_param = NULL;
      foreach ($query_params as $key => $param) {
        if (strpos($param, '=date:') !== FALSE) {
          $date_param_exists = TRUE;
          $date_param = $param;
          unset($query_params[$key]);
          $query_params = array_values($query_params);
        }
      }

      if ($date_param_exists) {
        // Date filter is in the URL.
        $new_param = explode(':', $date_param);

        if ($form_state->getValue('end')) {
          $end = $form_state->getValue('end');
          $string = $new_param[0] . ':start+' . $start . '+end+' . $end;
        }
        else {
          $string = $new_param[0] . ':start+' . $start;
        }

        $query_params[count($query_params)] = $string;
      }
      else {
        // Date filter is not in the URL.
        $index = [];
        foreach ($query_params as $param) {
          $param_index = explode('=', $param);
          $index[] = (strpos($param, 'f[') !== FALSE) ? $param_index[0] : NULL;
        }

        $index = array_filter($index);

        if ($form_state->getValue('end')) {
          $end = $form_state->getValue('end');
          $query_params[count($query_params)] = 'f[' . (count($index)) . ']=date:start+' . $start . '+end+' . $end;
        }
        else {
          $query_params[count($query_params)] = 'f[' . (count($index)) . ']=date:start+' . $start;
        }
      }

      $final_params = [];
      foreach ($query_params as $param) {
        $param = explode('=', $param);
        $final_params[$param[0]] = $param[1];
      }
      $final_params = array_filter($final_params);

      // Ensure to pass node id as route param
      // if route corresponds to a node.
      $node = \Drupal::routeMatch()->getParameter('node');

      if (!empty($node) && $node->id()) {
        $form_state->setRedirect(\Drupal::routeMatch()->getRouteName(), ['node' => $node->id()], ['query' => $final_params]);
      }
      else {
        $form_state->setRedirect(\Drupal::routeMatch()->getRouteName(), [], ['query' => $final_params]);
      }
    }
  }

  /**
   * Generates URL for the clear date filter values link.
   *
   * @return string
   *   URL.
   */
  private function getClearDatesUrl() {
    // Generate "Clear Dates" link.
    $requestUri = \Drupal::request()->getRequestUri();
    $path = parse_url($requestUri, PHP_URL_PATH);
    $queryParams = explode('&', parse_url(urldecode($requestUri), PHP_URL_QUERY));
    foreach ($queryParams as $key => $p) {
      if (strpos($p, 'date:') !== FALSE) {
        unset($queryParams[$key]);
      }
    }

    $queryString = (!empty(implode('&', $queryParams))) ? '?' . implode('&', $queryParams) : NULL;

    return $path . $queryString;
  }

}
