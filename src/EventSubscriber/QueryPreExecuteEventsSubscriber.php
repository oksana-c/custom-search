<?php

namespace Drupal\custom_search\EventSubscriber;

use Datetime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Event\QueryPreExecuteEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\custom_search\EventSubscriber
 */
class QueryPreExecuteEventsSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiEvents::QUERY_PRE_EXECUTE => 'queryAlter',
    ];
  }

  /**
   * React to an item being indexed.
   *
   * @param \Drupal\search_api\Event\QueryPreExecuteEvent $event
   *   Search API Indexing Items event.
   */
  public function queryAlter(QueryPreExecuteEvent $event) {
    if ($view = views_get_current_view()) {
      if ($view->id() == 'database_of_rules') {
        $queries = \Drupal::request()->query->all();
        if ((!empty($queries['effective_start_date']) &&
          $this->validDate($queries['effective_start_date'])) ||
          (!empty($queries['effective_end_date']) &&
            $this->validDate($queries['effective_end_date']))) {
          $query = $event->getQuery();
          $conditions = $query->createConditionGroup('AND');
          if (!empty($queries['effective_start_date']) &&
            $this->validDate($queries['effective_start_date'])) {
            $conditions->addCondition('field_effective_date', $queries['effective_start_date'], '>=');
          }
          if (!empty($queries['effective_end_date']) &&
            $this->validDate($queries['effective_end_date'])) {
            $conditions->addCondition('field_effective_date', $queries['effective_end_date'], '<=');
          }
          $query->addConditionGroup($conditions);
        }
        if ((!empty($queries['received_start_date']) &&
          $this->validDate($queries['received_start_date'])) ||
          (!empty($queries['received_end_date']) &&
            $this->validDate($queries['received_end_date']))) {
          $query = $event->getQuery();
          $conditions = $query->createConditionGroup('AND');
          if (!empty($queries['received_start_date']) &&
            $this->validDate($queries['received_start_date'])) {
            $conditions->addCondition('field_received', $queries['received_start_date'], '>=');
          }
          if (!empty($queries['received_end_date']) &&
            $this->validDate($queries['received_end_date'])) {
            $conditions->addCondition('field_received', $queries['received_end_date'], '<=');
          }
          $query->addConditionGroup($conditions);
        }
      }
    }
  }

  /**
   * Validates date.
   */
  private function validDate($date, $format = 'Y-m-d') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
  }

}
