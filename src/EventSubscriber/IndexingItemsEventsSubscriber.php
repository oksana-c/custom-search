<?php

namespace Drupal\custom_search\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\custom_search\ContentTypeMapInterface;
use Drupal\node\NodeInterface;
use Drupal\search_api\Event\IndexingItemsEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\search_api\Item\FieldInterface;
use Drupal\search_api\Item\Item;
use Drupal\taxonomy\TermStorage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\custom_search\EventSubscriber
 */
class IndexingItemsEventsSubscriber implements EventSubscriberInterface, ContentTypeMapInterface {

  /**
   * Constant for an array of 3 US Gov Branch labels.
   */
  const US_GOV_BRANCHES = [
    'Executive Branch',
    'Legislative Branch',
    'Judicial Branch',
  ];

  /**
   * Constant for the term id for Appropriations Decision.
   */
  const APPROPRIATIONS_DECISION = 981;

  /**
   * Constant for the term id for Bid Protest Decision.
   */
  const BID_PROTEST_DECISION = 971;

  /**
   * Constant for the term id for Federal Agency Major Rule Report.
   */
  const FEDERAL_AGENCY_MAJOR_RULE_REPORT = 1006;

  /**
   * Constant for the term id for Recommendation status Open.
   */
  const REC_STATUS_OPEN_TID = 631;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * Array of top level agencies.
   *
   * @var array
   */
  protected $topLevelAgencyTree = [];

  /**
   * Array of top level agencies by term id.
   *
   * @var array
   */
  protected $topLevelAgencyByTid = [];

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager interface.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->topLevelAgencyTree = $this->buildTopLevelTree();
  }

  /**
   * Builds an array of top level term ids below the three branches.
   */
  public function buildTopLevelTree() {
    $tree = &drupal_static(__METHOD__);
    if (!$tree) {
      foreach (self::US_GOV_BRANCHES as $branch) {
        $tid = $this->getTidByName($branch, 'agency');
        $children = $this->entityTypeManager->getStorage('taxonomy_term')->loadChildren($tid, 'agency');
        foreach (array_keys($children) as $childTid) {
          $tree[$childTid] = $tid;
        }
      }
    }
    return $tree;
  }

  /**
   * Utility: find term by name and vid.
   */
  protected function getTidByName($name = NULL, $vid = NULL) {
    $properties = [];
    if (!empty($name)) {
      $properties['name'] = $name;
    }
    if (!empty($vid)) {
      $properties['vid'] = $vid;
    }
    $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadByProperties($properties);
    $term = reset($terms);

    return !empty($term) ? $term->id() : 0;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiEvents::INDEXING_ITEMS => 'alterItems',
    ];
  }

  /**
   * React to an item being indexed.
   *
   * @param \Drupal\search_api\Event\IndexingItemsEvent $event
   *   Search API Indexing Items event.
   */
  public function alterItems(IndexingItemsEvent $event) {
    $items = $event->getItems();

    foreach ($items as $item) {

      $entity_type = $item->getDatasourceId();

      if (!$entity_type) {
        continue;
      }

      $agency_term_names_field = $item->getField('agency_term_names');
      $content_type_facet_field = $item->getField('content_type_facet');
      $topic_facet_field = $item->getField('topic_facet');
      $agency_term_top_level_field = $item->getField('agency_term_top_level');
      $subagency_or_agency_term_field = $item->getField('subagency_or_agency_term');
      $rec_subagency_term = $item->getField('rec_subagency_term');
      $product_has_open_recs_field = $item->getField('product_has_open_recs');
      $product_has_priority_recs_field = $item->getField('product_has_priority_recs');
      $rendered_item_field = $item->getField('rendered_item');

      // Set values for the primary Agency Facet.
      if ($agency_term_names_field) {
        $this->setAgencyFacetValues($item, $entity_type, $agency_term_names_field);
      }

      // Set values for the primary Content Type Facet.
      if ($content_type_facet_field && in_array($entity_type, ['entity:node', 'entity:media'])) {
        $this->setContentTypeFacetValues($item, $entity_type, $content_type_facet_field);
      }

      // Set values for the Topic Facet on Basic and
      // Reports & Testimonies search.
      if ($topic_facet_field) {
        $this->setTopicFacetValues($item, $entity_type, $topic_facet_field);
      }

      if ($entity_type == 'entity:node') {
        $node = $item->getOriginalObject()->getValue();
        if ($node instanceof NodeInterface) {
          $bundle = $node->bundle();
          if ($bundle == 'product') {
            $this->setAutocompleteFields($item, $node);

            // Set value for has open recs boolean.
            if ($product_has_open_recs_field) {
              $this->setOpenRecsValue($item, $product_has_open_recs_field);
            }
            // Set value for has priority recs boolean.
            if ($product_has_priority_recs_field) {
              $this->setPriorityRecsValue($item, $product_has_priority_recs_field);
            }
            // Set top level term.
            if ($agency_term_top_level_field) {
              $this->setTopLevelAgency($item, $agency_term_top_level_field);
            }
          }

          if ($bundle == 'action_tracker_area') {
            $this->setDocdate($item, $node);
          }
          elseif ($bundle == 'bid_protest_docket' ||
          $bundle == 'federal_vacancies' ||
          $bundle == 'federal_rules') {
            // Set top level term.
            if ($agency_term_top_level_field) {
              $this->setTopLevelAgency($item, $agency_term_top_level_field);
            }
            // Set values for the primary Agency Facet.
            if ($subagency_or_agency_term_field) {
              $this->setSubAgency($item, $subagency_or_agency_term_field);
            }
          }
          elseif ($bundle == 'recommendations') {
            if ($agency_term_top_level_field) {
              $this->setTopLevelAgency($item, $agency_term_top_level_field);
            }
            if ($rec_subagency_term) {
              $this->setSubAgency($item, $rec_subagency_term);
            }
          }
          elseif ($bundle == 'blog') {
            $this->setParentProductNumberForBlog($item, $rendered_item_field);
          }
        }
      }

      // Set values for the primary Content Type Facet.
      if ($rendered_item_field && $entity_type == 'entity:media') {
        $this->setParentProductNumberForMedia($item, $rendered_item_field);
      }
    }

    $event->setItems($items);
  }

  /**
   * Set values for indexed aggregated field agency_term_names.
   *
   * Values of this field are used to build "By Agency" Facet.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  public function setSubAgency(Item &$item, FieldInterface $field) {
    $terms = [];
    $values = $field->getValues();
    foreach ($values as $tid) {
      // Include the term if it is not a top level agency.
      if (!array_key_exists($tid, $this->topLevelAgencyTree)) {
        $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($tid);
        $terms[] = $term->getName();
      }
    }
    $field->setValues($terms);
  }

  /**
   * Set values for indexed aggregated field agency_term_names.
   *
   * Values of this field are used to build "By Agency" Facet.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  public function setTopLevelAgency(Item &$item, FieldInterface $field) {
    $parentAgencies = [];
    $values = $field->getValues();
    foreach ($values as $tid) {
      if (isset($this->topLevelAgencyByTid[$tid])) {
        $parentAgencies[] = $this->topLevelAgencyByTid[$tid];
      }
      else {
        $parents = $this->entityTypeManager->getStorage('taxonomy_term')->loadAllParents($tid);
        foreach ($parents as $parent) {
          // The parent is in the top level below the US_GOV_BRANCHES.
          if (array_key_exists($parent->id(), $this->topLevelAgencyTree)) {
            // Save this for next time.
            $this->topLevelAgencyByTid[$tid] = $parent->getName();
            $parentAgencies[] = $parent->getName();
          }
        }
      }
    }
    $field->setValues($parentAgencies);
  }

  /**
   * Set values for field that provide autocomplete content.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\node\NodeInterface $node
   *   Indexed item node.
   */
  public function setAutocompleteFields(Item $item, NodeInterface $node) {
    // Set values for field that provide autocomplete content
    // for search blocks.
    // Search Decisions.
    if ($decisionField = $item->getField('rendered_product_bid_protest_decision')) {
      $this->filterByTaxonomy($item, $node, $decisionField, self::BID_PROTEST_DECISION);
    }
    // Search Major Rules.
    if ($majorField = $item->getField('rendered_product_major_rule_report')) {
      $this->filterByTaxonomy($item, $node, $majorField, self::FEDERAL_AGENCY_MAJOR_RULE_REPORT);
    }
    // Search Appropriations Decision.
    if ($majorField = $item->getField('rendered_product_appropriations_decision')) {
      $this->filterByTaxonomy($item, $node, $majorField, self::APPROPRIATIONS_DECISION);
    }
  }

  /**
   * Remove field value if node does not contain a term id.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\node\NodeInterface $node
   *   Indexed item node.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   * @param int $targetTid
   *   Term id to filter with.
   * @param string $termField
   *   Node field to filter with.
   */
  public function filterByTaxonomy(Item &$item, NodeInterface $node, FieldInterface $field, int $targetTid, string $termField = 'field_document_type') {
    if ($node->hasField($termField)) {
      $vals = $node->get($termField)->getValue();
      $tids = [];
      foreach ($vals as $tid) {
        $tids[] = $tid['target_id'];
      }
      if (!in_array($targetTid, $tids)) {
        // Set value to null for this field if Product does not contain TID.
        $field->setValues([]);
      }
    }
  }

  /**
   * Returns an array of term's parent terms.
   *
   * @param \Drupal\taxonomy\TermStorage $storage
   *   Term entity storage.
   * @param string $tid
   *   The TID of an Agency term.
   *
   * @return array
   *   An array of Agency term objects keyed by Term name.
   */
  protected function getParentAgencies(TermStorage $storage, $tid) {
    $parent_agencies = [];
    $parents = $storage->loadParents($tid);
    foreach ($parents as $parent) {
      $parent_agencies[$parent->getName()] = $parent;
    }

    return $parent_agencies;
  }

  /**
   * Set values for indexed aggregated field agency_term_names.
   *
   * Values of this field are used to build "By Agency" Facet.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param string $entity_type
   *   Entity type of the indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  protected function setAgencyFacetValues(Item &$item, string $entity_type, FieldInterface $field) {
    $parent_terms = [];
    $parent_agencies = [];
    $storage_term = $this->entityTypeManager->getStorage('taxonomy_term');

    $top_level_agency_names = array_keys($this->getTopLevelAgencies());

    // Process Node entities. Agency fields are available on most nodes.
    // agency_term_names field will include field_agency and
    // field_subagency values for nodes on which these fields are filled.
    if ($entity_type === 'entity:node') {
      $tids = [];
      $node = $item->getOriginalObject()->getValue();

      // Action Tracker Area Nodes inherit Agencies from all
      // Child Action Tracker nodes.
      // @todo: reindex Parent Action Tracker Area when child Action is added/updated.
      if ($node->bundle() == 'action_tracker_area') {
        $storage_node = $this->entityTypeManager->getStorage('node');
        $query = \Drupal::entityQuery('node')
          ->condition('status', NodeInterface::PUBLISHED)
          ->condition('type', 'action_tracker')
          ->condition('field_area_name.target_id', $node->id())
          ->Exists('field_implementing_entity');

        $nids = $query->execute();

        foreach ($nids as $nid) {
          $action = $storage_node->load($nid);
          $implementing_entities = $action->get('field_implementing_entity')->getValue();

          foreach ($implementing_entities as $target_id) {
            $tids[] = $target_id['target_id'];
          }

        }
        $tids = array_unique(array_filter($tids));
      }

      $values = $field->getValues() ? $field->getValues() : $tids;

      foreach ($values as $tid) {
        $parent_agencies = $this->getParentAgencies($storage_term, $tid);

        $parent_agency_term_names = array_keys($parent_agencies);
        $term = $storage_term->load($tid);

        // Parent Term names, excluding Branches.
        $intersection = array_intersect($top_level_agency_names, $parent_agency_term_names);

        // Add all term parents to field.
        if (in_array($term->getName(), $top_level_agency_names)) {
          array_push($parent_terms, $term->getName());
        }
        $parent_terms = array_merge($parent_terms, $intersection);
      };
    }

    // For media items, query Parent product nodes using field_parent_ids
    // value and assign parent node Agencies at the time of indexing.
    if ($entity_type === 'entity:media') {
      $parent_ids = $item->getField('field_parent_ids')->getValues();
      if (!$parent_ids) {
        return;
      }

      $storage_node = $this->entityTypeManager->getStorage('node');
      $parent_ids = explode(',', reset($parent_ids));

      foreach ($parent_ids as $content_id) {
        $content_id = trim($content_id);
        $node = $storage_node->loadByProperties(['field_content_id' => $content_id]);
        if (empty($node)) {
          continue;
        }
        $node = reset($node);

        if ($node->hasField('field_agency')) {
          $field_agency = $node->get('field_agency')->getValue();
          if ($field_agency) {
            foreach ($field_agency as $tid) {
              $tid = $tid['target_id'];
              $term = $storage_term->load($tid);
              $parent_agencies = array_merge($parent_agencies, $this->getParentAgencies($storage_term, $tid));

              if (in_array($term->getName(), $top_level_agency_names)) {
                array_push($parent_terms, $term->getName());
              }

            }
          }
        }
        if ($node->hasField('field_subagency')) {
          $field_subagency = $node->get('field_subagency')->getValue();
          if ($field_subagency) {
            foreach ($field_subagency as $tid) {
              $tid = $tid['target_id'];
              $term = $storage_term->load($tid);
              $parent_agencies = array_merge($parent_agencies, $this->getParentAgencies($storage_term, $tid));

              if (in_array($term->getName(), $top_level_agency_names)) {
                array_push($parent_terms, $term->getName());
              }
            }
          }
        }

        $parent_agency_term_names = array_keys($parent_agencies);
        // Parent Term names, excluding Branches.
        $intersection = array_intersect($top_level_agency_names, $parent_agency_term_names);
        $parent_terms = array_merge($parent_terms, $intersection);
      }

    }

    $final_parents = array_unique($parent_terms);

    // Set agency_term_names field value for indexing.
    if ($final_parents) {
      $field->setValues($final_parents);
    }
    else {
      $field->setValues([NULL]);
    }
  }

  /**
   * Return an array of top level Agency terms.
   *
   * Returned terms are immediate children of 3 US Gov Brnaches.
   *
   * @return array
   *   An array of top level Agency terms keyed by Agency name.
   */
  protected function getTopLevelAgencies() {
    $top_level_agency_terms = [];
    $storage_term = $this->entityTypeManager->getStorage('taxonomy_term');

    // Get 1st and  2nd level of taxonomy terms.
    $tree = $storage_term->loadTree('agency', 0, 2, TRUE);

    foreach ($tree as $tree_term) {
      // We get 1st and 2nd levels, also we check parents
      // (only 2nd level has parents).
      // We exclude terms from the 1st level that are not within
      // the 3 main branches of government.
      $parents = $storage_term->loadParents($tree_term->id());

      if (!empty($parents)) {
        foreach ($parents as $parent) {
          if (in_array($parent->getName(), self::US_GOV_BRANCHES)) {
            $top_level_agency_terms[$tree_term->getName()] = $tree_term;
          }
        }
      }
    }

    return $top_level_agency_terms;
  }

  /**
   * Set values for indexed aggregated field content_type_facet.
   *
   * Values of this field are used to build "Content Type" Facet.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param string $entity_type
   *   Entity type of the indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  protected function setContentTypeFacetValues(Item &$item, string $entity_type, FieldInterface $field) {
    $values = [];
    if ($entity_type === 'entity:node') {
      $content_type = $item->getField('type')->getValues()[0];
      if ($field->getValues() && $content_type === 'product') {
        foreach ($tids = $field->getValues() as $tid) {
          if (isset(self::PRODUCT_TYPE_MAPPING[$tid])) {
            $tid_mapping = self::PRODUCT_TYPE_MAPPING[$tid];
            $values = $tid_mapping ? array_merge($tid_mapping, $values) : [NULL];
          }
          else {
            $values = [NULL];
          }
        }
      }
      else {
        $bundle_mapping = self::BUNDLE_TYPE_MAPPING[$content_type];
        $values = $bundle_mapping ? array_merge($bundle_mapping, $values) : [NULL];
      }
    }
    if ($entity_type === 'entity:media') {
      $bundle = $item->getField('bundle')->getValues()[0];
      $values = $bundle ? self::BUNDLE_TYPE_MAPPING[$bundle] : [NULL];
    }

    $field->setValues($values);
  }

  /**
   * Set value of docdate field to use in date facet.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\node\NodeInterface $node
   *   Indexed item node.
   */
  public function setDocdate(Item $item, NodeInterface $node) {
    // Set docdate value based on last updated date.
    if ($docdate = $item->getField('docdate')) {
      $date = $node->get('changed')->getValue()[0]['value'];
      $docdate->setValues([$date]);
    }
  }

  /**
   * Set values for indexed aggregated field topic_facet.
   *
   * Values of this field are used to build "Topic" Facet
   * on Basic Search and Reports & Testimonies Search pages.
   *
   * Values need to be set custom only for Media entities,
   * since they inherit topics from parent Products.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param string $entity_type
   *   Entity type of the indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  protected function setTopicFacetValues(Item &$item, string $entity_type, FieldInterface $field) {
    $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $topics = [];

    if ($entity_type === 'entity:node') {
      $tids = $item->getField('topic_facet')->getValues();
      if ($tids) {
        foreach ($tids as $tid) {
          $term = $storage->load($tid);
          if ($term) {
            $topics[] = $term->label();
          }
        }
      }
    }

    if ($entity_type === 'entity:media') {
      $parent_ids = $item->getField('field_parent_ids')->getValues();
      if (!$parent_ids) {
        return;
      }

      $storage_node = $this->entityTypeManager->getStorage('node');
      $parent_ids = explode(',', reset($parent_ids));

      foreach ($parent_ids as $content_id) {
        $content_id = trim($content_id);
        $node = $storage_node->loadByProperties(['field_content_id' => $content_id]);
        if (empty($node)) {
          continue;
        }
        $node = reset($node);

        if ($node->hasField('field_topic')) {
          $field_topic = $node->get('field_topic')->getValue();
          foreach ($field_topic as $tid) {
            $term = $storage->load($tid['target_id']);
            $topics[] = $term->label();
          }
        }
        if ($node->hasField('field_additional_topics')) {
          $field_additional_topics = $node->get('field_additional_topics')->getValue();
          foreach ($field_additional_topics as $tid) {
            $term = $storage->load($tid['target_id']);
            $topics[] = $term->label();
          }
        }
      }
    }

    $final_topics = array_unique($topics);

    // Set topics field value for indexing.
    $field->setValues($final_topics);
  }

  /**
   * Set Product Number values for Child Media items.
   *
   * Product Number values will be added to rendered_item
   * indexed field, to ensure that Media entities are
   * showing up in search for specific Product Number.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  protected function setParentProductNumberForMedia(Item &$item, FieldInterface $field) {
    $product_numbers = [];

    $parent_ids = $item->getField('field_parent_ids')->getValues();
    if (!$parent_ids) {
      return;
    }

    $storage_node = $this->entityTypeManager->getStorage('node');
    $parent_ids = explode(',', reset($parent_ids));

    foreach ($parent_ids as $content_id) {
      $content_id = trim($content_id);
      $node = $storage_node->loadByProperties(['field_content_id' => $content_id]);
      if (empty($node)) {
        continue;
      }
      $node = reset($node);

      if ($node->hasField('field_product_number')) {
        $field_product_number = $node->get('field_product_number')->getString();
        $product_numbers[] = $field_product_number;
      }
    }

    // Set additional rendered_item field value for indexing.
    $field->addValue(array_unique($product_numbers));
  }

  /**
   * Set Product Number values for Blogs related to Products.
   *
   * Product Number values will be added to rendered_item
   * indexed field, to ensure that Blog nodes are
   * showing up in search for specific Product Number.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  protected function setParentProductNumberForBlog(Item &$item, FieldInterface $field) {
    $product_numbers = [];

    $related_products = $item->getField('field_products')->getValues();
    if (!$related_products) {
      return;
    }

    $storage_node = $this->entityTypeManager->getStorage('node');

    foreach ($related_products as $nid) {
      $node = $storage_node->loadByProperties(['nid' => $nid]);
      if (empty($node)) {
        continue;
      }
      $node = reset($node);

      if ($node->hasField('field_product_number')) {
        $field_product_number = $node->get('field_product_number')->getString();
        $product_numbers[] = $field_product_number;
      }
    }

    // Set additional rendered_item field value for indexing.
    $field->addValue(array_unique($product_numbers));
  }

  /**
   * Set value for indexed aggregated field product_has_open_recs.
   *
   * Value of this field is used to filter Open Recs view.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  public function setOpenRecsValue(Item &$item, FieldInterface $field) {
    // Get product node.
    $product_node = $item->getOriginalObject()->getValue();
    // Get all recommendations from this product that are 'open'.
    $query = \Drupal::entityQuery('node')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'recommendations')
      ->condition('field_product.target_id', $product_node->id())
      ->condition('field_status_code.target_id', self:: REC_STATUS_OPEN_TID);
    $nids = $query->execute();
    // If they exist, open recs value is true.
    $has_open_recs = (!empty($nids)) ? TRUE : FALSE;
    $field->setValues([$has_open_recs]);
  }

  /**
   * Set value for indexed aggregated field product_has_open_recs.
   *
   * Value of this field is used to filter Open Recs view.
   *
   * @param \Drupal\search_api\Item\Item $item
   *   Indexed item.
   * @param \Drupal\search_api\Item\FieldInterface $field
   *   Indexed item field.
   */
  public function setPriorityRecsValue(Item &$item, FieldInterface $field) {
    // Get product node.
    $product_node = $item->getOriginalObject()->getValue();
    // Get all recommendations from this product that are 'priority'.
    $query = \Drupal::entityQuery('node')
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'recommendations')
      ->condition('field_product.target_id', $product_node->id())
      ->condition('field_status_code.target_id', self:: REC_STATUS_OPEN_TID)
      ->condition('field_priority_indicator.value', 1);
    $nids = $query->execute();
    // If they exist, priority recs value is true.
    $has_priority_recs = (!empty($nids)) ? TRUE : FALSE;
    $field->setValues([$has_priority_recs]);
  }

}
