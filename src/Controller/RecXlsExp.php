<?php

namespace Drupal\custom_search\Controller;

use Drupal\Component\Utility\Html;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Drupal\views\Views;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Defines a route controller for importing action tracker items.
 */
class RecXlsExp extends ControllerBase {

  /**
   * Format to write XLS files as.
   *
   * @var string
   */
  protected $xlsFormat = 'Xlsx';

  /**
   * Provides xls file.
   */
  public function index() {
    $view_id = 'open_recommendations';
    $display_id = 'data_export_xlsx';
    $view = Views::getView($view_id);
    // Set to zero so all results are loaded.
    $view->setItemsPerPage(0);
    $view->setDisplay($display_id);
    $view->preExecute();
    $view->execute();
    $result = $view->preview();
    $data = json_decode($result['#markup']->jsonSerialize());

    $xls = new Spreadsheet();
    $xls->setActiveSheetIndex(0);
    $sheet = $xls->getActiveSheet();

    // Set headers.
    $headers = [
      'Publication Name',
      'Publication',
      'Number Date Publication Issued Director Name Director Phone',
      'Agency',
      'Recommendation',
      'Status',
      'Priority',
      'Comments',
    ];
    foreach ($headers as $column => $header) {
      $sheet->setCellValueByColumnAndRow(++$column, 1, $header);
    }
    $date = date('M d, Y', time());
    $time = date('g:i A', time());
    $additional = [
      [
        'title' => t('Title: Download of CLIENT Recommendation Results'),
      ],
      [
        'title' => t('Prepared by: CLIENT'),
      ],
      [
        'title' => t('Source: CLIENT recommendations database, status as of @date at @time EST', ['@date' => $date, '@time' => $time]),
      ],
      [],
      [],
    ];

    $data = array_merge($additional, $data);
    // Set the data.
    $this->setData($sheet, $data);

    // Set the width of every column with data in it to AutoSize.
    $this->setColumnsAutoSize($sheet);

    // Setup an empty response, so for example, the Content-Disposition header
    // can be set.
    $response = new Response();

    $writer = IOFactory::createWriter($xls, $this->xlsFormat);
    ob_start();
    $writer->save('php://output');
    $output = ob_get_clean();

    $response->setContent($output);

    if ($filename = $view->getDisplay()->getOption('filename')) {
      $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');
    }
    $response->headers->set('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    return $response;
  }

  /**
   * Set width of all columns with data in them in sheet to AutoSize.
   *
   * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
   *   The worksheet to set the column width to AutoSize for.
   */
  protected function setColumnsAutoSize(Worksheet $sheet) {
    foreach ($sheet->getColumnIterator() as $column) {
      $column_index = $column->getColumnIndex();
      $sheet->getColumnDimension($column_index)->setAutoSize(TRUE);
    }
  }

  /**
   * Set sheet data.
   *
   * @param \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet $sheet
   *   The worksheet to put the data in.
   * @param array $data
   *   The data to be put in the worksheet.
   */
  protected function setData(Worksheet $sheet, array $data) {
    foreach ($data as $i => $row) {
      $column = 1;
      foreach ($row as $value) {
        // Since headers have been added, rows are offset here by 2.
        $sheet->setCellValueByColumnAndRow($column, $i + 2, $this->formatValue($value));
        $column++;
      }
    }
  }

  /**
   * Formats a single value for a given XLS cell.
   *
   * @param string $value
   *   The raw value to be formatted.
   *
   * @return string
   *   The formatted value.
   */
  protected function formatValue($value) {
    // @todo Make these filters configurable.
    $value = Html::decodeEntities($value);
    $value = strip_tags($value);
    $value = trim($value);

    return $value;
  }

}
