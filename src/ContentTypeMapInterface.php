<?php

namespace Drupal\custom_search;

/**
 * Interface ContentTypeMap.
 *
 * @package Drupal\custom_search
 */
interface ContentTypeMapInterface {

  /**
   * Content Type Facet mapping per Product Type Vocabulary Term.
   */
  const PRODUCT_TYPE_MAPPING = [
    // Reports & Testimonies > Report.
    '916' => [
      'Reports & Testimonies',
      'Report',
    ],
    // Reports & Testimonies > Report > Technology Assessment.
    '921' => [
      'Reports & Testimonies',
      'Report',
    ],
    // Reports & Testimonies > Report > Priority Recommendation Letter.
    '951' => [
      'Reports & Testimonies',
      'Report',
    ],
    // Reports & Testimonies > Testimony.
    '926' => [
      'Reports & Testimonies',
      'Testimony',
    ],
    // Reports & Testimonies > Comment Letter.
    '931' => [
      'Reports & Testimonies',
      'Comment Letter',
    ],
    // Reports & Testimonies > Memorandum.
    '936' => [
      'Reports & Testimonies',
      'Memorandum',
    ],
    // Reports & Testimonies > Other Written Product.
    '941' => [
      'Reports & Testimonies',
      'Other Product',
    ],
    // Legal > Bid Protest > Bid Protest Decision.
    '971' => [
      'Bid Protest',
      'Bid Protest Decision',
    ],
    // Legal > Appropriations Law > Appropriations Decision.
    '981' => [
      'Appropriations Law',
      'Appropriations Decision',
    ],
    // Legal > Appropriations Law > Antideficiency Act Report.
    '986' => [
      'Appropriations Law',
      'Antideficiency Act Report',
    ],
    // Legal > Other Legal Function > Federal Agency Major Rule Report.
    '1006' => [
      'Other Legal Function',
      'Federal Agency Major Rule Report',
    ],
    // Legal > Other Legal Function > Federal Vacancy Reform Act >
    // > Federal Vacancies Violation Letter.
    '1001' => [
      'Other Legal Function',
      'Federal Vacancy Reform Act',
      'Federal Vacancies Violation Letter',
    ],
    // Legal > Other Legal Function > Federal Vacancy Reform Act >
    // > Federal Vacancies Reminder Letter.
    '996' => [
      'Other Legal Function',
      'Federal Vacancy Reform Act',
      'Federal Vacancies Reminder Letter',
    ],
    // Legal > Other Legal Function > Federal Vacancy Reform Act >
    // > Federal Vacancies Audit Report.
    '81221' => [
      'Other Legal Function',
      'Federal Vacancy Reform Act',
      'Federal Vacancies Audit Report',
    ],
    // Legal > Other Legal Function > Legal Report.
    // @todo: Label "Legal Report" will be modified in facet preprocessor. Add reference.
    '1021' => [
      'Other Legal Function',
      'Legal Report',
    ],
    // Legal > Other Legal Function > Legal Testimony.
    // @todo: Label "Legal Testimony" will be modified in facet preprocessor. Add reference.
    '1026' => [
      'Other Legal Function',
      'Legal Testimony',
    ],
    // Legal > Other Legal Function > Other Decision.
    '1031' => [
      'Other Legal Function',
      'Other Decision',
    ],
    // Legal > Other Legal Function > Legal Other Written Product.
    // @todo: Label "Legal Other Written Product" will be modified in facet preprocessor. Add reference.
    '1036' => [
      'Other Legal Function',
      'Legal Other Written Product',
    ],
    // Other > Comptroller General Presentation.
    '1046' => [
      'Other',
      'Comptroller General Presentation',
    ],
    // Other > OIG Product > OIG Report.
    '1056' => [
      'Other',
      'OIG Product',
      'OIG Report',
    ],
    // Other > OIG Product > OIG Testimony.
    '1061' => [
      'Other',
      'OIG Product',
      'OIG Testimony',
    ],
    // Other > OIG Product > OIG Testimony.
    '1066' => [
      'Other',
      'OIG Product',
      'Other OIG Product',
    ],
  ];

  /**
   * Content Type Facet mapping per entity bundle.
   */
  const BUNDLE_TYPE_MAPPING = [
    // Bid Protest Docket nodes.
    'recommendations' => [
      'Reports & Testimonies',
      'Recommendation',
    ],
    // Bid Protest Docket nodes.
    'bid_protest_docket' => [
      'Bid Protest',
      'Bid Protest Docket',
    ],
    // Federal Rule nodes.
    'federal_rules' => [
      'Other Legal Function',
      'Federal Rule',
    ],
    // Remote Video.
    'remote_video' => [
      'Other',
      'Video',
    ],
    // Podcast.
    'podcast' => [
      'Other',
      'Podcast',
    ],
    // Infographic.
    'infographic' => [
      'Other',
      'Infographic',
    ],
    // Interactive Graphic.
    'interactive_graphic' => [
      'Other',
      'Interactive Graphic',
    ],
    // Blog Post.
    'blog' => [
      'Other',
      'Blog Posts',
    ],
    // Press Release.
    'press_release' => [
      'Other',
      'Press Release',
    ],
    // Staff Profile.
    'staff_profile' => [
      'Other',
    ],
  ];

  /**
   * Content Type Hierarchy / Order for the Facet.
   */
  const CONTENT_TYPE_HIERARCHY = [
    'Reports & Testimonies' => [
      'Report' => [],
      'Testimony' => [],
      'Recommendation' => [],
      'Comment Letter' => [],
      'Memorandum' => [],
      'Other Product' => [],
    ],
    'Bid Protest' => [
      'Bid Protest Decision' => [],
      'Bid Protest Docket' => [],
    ],
    'Appropriations Law' => [
      'Appropriations Decision' => [],
      'Antideficiency Act Report' => [],
    ],
    'Other Legal Function' => [
      'Federal Agency Major Rule Report' => [],
      'Federal Rule' => [],
      'Federal Vacancy Reform Act' => [
        'Federal Vacancies Violation Letter' => [],
        'Federal Vacancies Reminder Letter' => [],
        'Federal Vacancies Audit Report' => [],
      ],
      'Legal Report' => [],
      'Legal Testimony' => [],
      'Other Decision' => [],
      'Legal Other Written Product' => [],
    ],
    'Other' => [
      'Video' => [],
      'Podcast' => [],
      'Infographic' => [],
      'Interactive Graphic' => [],
      'Blog Posts' => [],
      'Press Release' => [],
      'Comptroller General Presentation' => [],
      'OIG Product' => [
        'OIG Report' => [],
        'OIG Testimony' => [],
        'Other OIG Product' => [],
      ],
    ],
  ];

}
